# Lookaside

Lookaside is a python package for working with the 128T look-aside model.


## Install

### Single File (prefered)

Use `wget` to pull in the current pre-built executable into the location of your choice. I use `/usr/bin` which requires an additional `sudo`, but you can place it anywhere. It just helps for it to be on your path. Update the permissions to be executable, and verify that it can be run.
```
wget https://bitbucket.org/gregschrock/lookaside/raw/7001455f0fb1d3916a74793655d01167b7ff55c0/bin/lookaside-0.2.0 -O /usr/bin/test-lookaside
chmod ugo+x /usr/bin/test-lookaside
/usr/bin/test-lookaside --help
```

### Pip
You can install the package using `pip`. It does require `python3.6`.
```
pip install git+https://bitbucket.org/gregschrock/lookaside.git@v0.2.0
```

## NEW

### Configuration

#### Default Flags

A feature added in 0.2 is the ability to specify default values for most of the flag arguments in the tool. For example, I prefer to build `--nocotire` and `--no-unity`. To set this to be the default behavior, simply configure that.

```
lookaside configure defaults cmake cotire no
lookaside configure defaults build unity no
```

These defaults will be reflected in the `--help` text for all relevant commands. For example, these particular flags will impact the `build`, `validate`, and `file-validate` subcommands.

The current configuration values can be checked by running specific commands.

```
lookaside show config defaults build unity
UNITY: Disabled
```

Or the entire config can be seen by:

```
$ lookaside show config
defaults:
  build:
    clang: false
    push: true
    unity: false
  cmake:
    cotire: false
  robot:
    report: true
  validate:
    file_only: true
    valgrind: false
```

#### Exception Reporting

**ACTION REQUESTED**

Contact me for the reporting URL. I thought it best not to hard-code it.

Also under the `configure` subcommand is the ability to enable reporting of issues with the tool. This was mostly a project of interest to me. As it stands, this will strictly report uncaught exceptions. Also note that the monitoring can be disabled at any time.

```
lookaside configure monitoring <reporting-url>
lookaside configure monitoring enable
```

## First Validation

There are a few steps required before the first build. Lookaside maintains the concepts of a `repo`, an `endpoint`, and an `association` between them (see Concept Definitions). The configuration of these dictates the bahavior of the module.

I'll begin by creating a repo for an imaginary `~/code/i95` directory. 

```
$ lookaside create repo --name i95 --path ~/code/i95
```

I can verify that the specified repo has been created.
```
$ lookaside show repos
Name    Path
------  ------------------------
i95     /Users/gschrock/code/i95
```

Now, I'll create an endpoint for my dev container (here relying on SSH config for "vm1").
```
$ lookaside create endpoint --name i95 --host vm1 --port 12800
```

Again, I can verify the existence of the new endpoint.
```
$ lookaside show endpoints
Name    Host      Port
------  ------  ------
i95     vm1      12800
```

I'll now associate those two in order to make operations within i95 impact that particular dev container.
```
$ lookaside create association --repo i95 --endpoint i95
```

Now, the relationship between the I95 directory and the development container is complete.
```
$ lookaside show associations
Repo    Endpoint
------  ----------
i95     i95
```

From within that directory, I can now validate a target. `lookaside validate` will, by default, first build a test target and then execute it. It also builds *without* unity by default, so keep that in mind.
```
$ cd ~/code/i95
$ lookaside validate libQueueTest
```


## Build Introspection

The `lookaside` tool uses common 128 practices to simplify finding and running unit tests. It allows operations like listing out the build target, unit test, and unit test target for a file.

```
$ lookaside describe /Users/gschrock/code/i95/src/apps/highway/HighwayManager/SessionManager.cpp
-----------  ------------------------------------------------------------------------------------
File         /Users/gschrock/code/i95/src/apps/highway/HighwayManager/SessionManager.cpp
Unit Test    /Users/gschrock/code/i95/src/apps/highway/HighwayManager/test/SessionManagerTest.cpp
Target       128T_highwayManager
Test Target  sessionManagerTest
-----------  ------------------------------------------------------------------------------------
```

A more commonly used feature (for me) is testing a particular file:

```
$ cd ~/code/i95/src/lib/queue
$ lookaside file-validate TankClient.hpp
# builds and runs libQueueTest
```


When working only on a particular file, the `--file-only` flag will filter tests to those within the related unit test.

```
$ cd ~/code/i95/src/lib/queue
$ lookaside file-validate --file-only TankClient.hpp
# builds and runs libQueueTest -f <every fixture in test/TankClientTest.cpp>
```


## Robot

I've created my own wrapper around running robot for my common use cases. It includes more understandable execution types that I find myself using. The complexity of the underlying robot flags is hidden.

Note: I do not yet support passing additional, arbitrary flags to the underlying robot invokation. I'm generally avoiding robot in favor of `topology128t` for deploying topologies (check it out). 

An example run would look like:
```
$ cd ~/code/dev
$ lookaside robot -t 116 -s robot/tests/suites/system/IPFIX/BasicIPFIX.robot --outdir results --run-type run
```

The initial run will require:
```
$ lookaside robot --run-type install ...
```
Note: `install` skips teardown

Subsequent runs will typically use:
```
$ lookaside robot --run-type run ...
```
Note: `run` goes straight to the test and skips teardown

This command will
1. Run robot from the associated endpoint
2. Pull the output from the endpoint to the specified `outdir`
3. Open the results in the default html application


## Miscilaneous

There are a couple other useful commands in there.

* `lookaside ssh`
    SSH to the associated endpoint (thanks athompson, I stole this from you)
* `lookaside clean-all`
    Run `download_wheels` and `cleanWeb` on the associated endpoint
* `lookaside run "ps -ef"`
    Run an arbitrary command on the associated endpoint

## Concept Definitions

### Repo

A `repo` is simply a directory. As in the typical lookaside model, a repo is assumed to have a corresponding directory on the target endpoint. Before any code can be used with the lookaside tool, its directory must be configured. Once configured, any `lookaside` commands run within that directory (or subdirectories) will act based on the configuration of that directory. If executed outside one of the configured `repo`s, `lookaside` will raise an error.

### Endpoint

An `endpoint` is a machine (or container) on which builds can take place. An `endpoint` must be configured before any builds can be attempted.

### Association

`lookaside` requires you to associate a `repo` with an `endpoint`. I personally have two I95 directories - each associated with its own container. So my builds for one always end up on the same container. The association can be easily updated to build on the alternative container, but I have avoided that in my workflow.

## Shortcomings

Internally, `lookaside` is still using `rpush`. The `rpush` script does not support a particular port. For containers, this means that an `rpush` will be performed to the associated endpoint host.

In the future, I want to replace the `rpush` logic, so the endpoints particular host won't matter, but for now it does.

## Disclaimer

I intend to change and improve the behavior of this module. Sorry if updates break some workflow. Please let me know if that's the case.
import functools
import pathlib

import attr
import click


import lookaside.operators.router as lookaside_router
from lookaside.config import config as lookaside_config


def _get_cwd():
    return pathlib.Path.cwd()


@attr.s
class EndpointContext:
    cwd = attr.ib(default=attr.Factory(_get_cwd))

    def get_name(slef):
        return 'endpoint'

    def get_value(self, ctx):
        if ctx.obj.selection is not None and ctx.obj.selection.endpoint is not None:
            return ctx.obj.selection.endpoint

        try:
            return _get_router().get_endpoint_for_path(self.cwd)
        except r_router.NoAssociatedRepo as e:
            click.echo(
                f'There is no repo for "{e.path}". Create one and try again.'
            )
            ctx.exit(1)
        except (r_router.NoSuchRepo, r_router.NoSuchEndpoint) as e:
            click.echo(f'{e} Create one and try again')
            ctx.exit(1)
        except r_router.NoAssociatedEndpoint as e:
            click.echo(
                f'There is no endpoint associated with "{e.repo_name}". '
                'Associate one and try again.'
            )
            ctx.exit(1)
        except r_router.InvalidAssociationException as e:
            click.echo(f'{e} Notify the maintainers.')
            ctx.exit(1)


def context_arg(cmd):
    def customizable_method(item):

        @click.pass_context
        def wrapping_cmd(ctx, *args, **kwargs):
            name = item.get_name()

            try:
                value = item.get_value(ctx)
            except Exception as e:
                click.echo(e)
                ctx.exit(1)

            kwargs[name] = value

            return ctx.invoke(cmd, *args, **kwargs)

        return functools.update_wrapper(wrapping_cmd, cmd)

    return customizable_method


def endpoint(cmd):
    return context_arg(cmd)(EndpointContext())


def _get_router():
    config_directory = _get_config_directory()
    router = lookaside_router.Router(config_directory)
    router._load_resources()

    return router


def _get_config_directory():
    return pathlib.Path(click.get_app_dir("lookaside"))


CONFIG = None
def get_config():
    global CONFIG

    if CONFIG is None:
        CONFIG = lookaside_config.Config.create_from_file(_get_config_directory() / 'config.json')

    return CONFIG

import pathlib
import sys

import click

from lookaside.cli.commands.vscode import tasks


@click.group()
def create():
    pass


@create.command(
    'tasks',
    help='Add some of the suggested lookaside VSCode tasks'
)
@click.option('--yes', '-y', is_flag=True, help='Add all tasks without asking')
@click.pass_context
def create_tasks(context, yes):
    root_command = context.find_root().info_name

    for subcommand in tasks._FILE_TASKS:
        command = subcommand.resolve(root_command)
        
        if yes or click.confirm(f'Add task "{command.name}"?'):
            try:
                tasks.set_build_task(command)
            except tasks.FailedToParseTasksFile as error:
                click.echo(f'ERROR: {error.message}')
                context.exit(1)

            click.echo(f'Added {command.name} to the list of tasks.')

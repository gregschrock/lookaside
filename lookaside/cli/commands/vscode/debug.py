import pathlib
import sys

import click

import lookaside.cli.commands.decorators as command

from lookaside.cli.commands.vscode import launch
from lookaside.codebase import cmake
from lookaside.codebase import file_relations
from lookaside import gtest


@click.command(
    'debug-file',
    help='Add a task debug configuration for the specified file'
)
@command.endpoint
@click.argument('path', 'target-file', type=click.Path(exists=True))
@click.option(
    '--file-only/--all',
    help="Filter to tests within this file's unit test (overriden by -f)",
    default=command.get_config().defaults.validate.file_only,
    show_default=True
)
@click.option('-f', help='Filter string', default=None, show_default=True)
def debug_file(endpoint, path, file_only, f):
    path = pathlib.Path(path)

    targets = [cmake.TestTarget.create_for_file(path)]

    if file_only and f is None:
        unit_test = file_relations.get_unit_test_path(path)
        f = gtest.filter.get_filters_for_tests_in(unit_test)

    if len(targets) == 0:
        click.echo(f'No target found for "{path}"')
        sys.exit(1)
    
    if len(targets) > 1:
        click.echo(f'Multiple targets found for "{path}": {[target.name for target in targets]}')
        sys.exit(1)

    target = targets[0]

    if not target.is_executable:
        click.echo(f'Found target {target.name}, but it is not an executable target')

    executable = file_relations.get_built_executable_path(target.directory / target.name)
    try:
        launch.set_launch_debug_config(endpoint, target.name, executable, f)
    except Exception as error:
        click.echo(f'FAILED: f{error}')
        sys.exit(1)
    
    click.echo(f'Added debug entry for {target.name}')

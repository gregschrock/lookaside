import contextlib
import json
import pathlib


from lookaside.operators import git


class FailedToParseLaunchFile(Exception):
    def __init__(self, path):
        return super().__init__(self, 'Failed to parse launch file "{path}" as JSON')


def set_launch_debug_config(endpoint, name, path, filter_string):
    launch = _load_launch_file()
    launch.setdefault('version', '0.2.0')
    configurations = launch.setdefault('configurations', [])

    configurations = list(filter(
        lambda configuration: configuration['name'] != name,
        configurations
    ))

    configurations.append(_create_configuration(endpoint, name, path, filter_string))
    launch['configurations'] = configurations

    _write_launch_file(launch)


def _create_configuration(endpoint, name, path, filter_string):
    return {
        "type": "cppdbg",
        "request": "launch",
        "name": name,
        "args": [
            f"'--gtest_filter'={filter_string}"
        ],
        'program': f'{path}',
        'internalConsoleOptions': 'openOnSessionStart',
        'cwd': '${workspaceRoot}',
        'pipeTransport': {
            'pipeCwd': '/usr/bin',
            'pipeProgram': '/usr/bin/ssh',
            'pipeArgs': [
                str(endpoint.host),
                '-p', str(endpoint.port)
            ],
            'debuggerPath': '/usr/bin/gdb'
        }
    }


def _load_launch_file():
    launch_path = _get_launch_path()

    if not launch_path.exists():
        return {}

    with _get_launch_path().open() as launch:
        try:
            return json.load(launch)
        except json.JSONDecodeError as error:
            raise FailedToParseLaunchFile(launch_path) from error


def _write_launch_file(content):
    with _get_launch_path().open('w') as launch:
        json.dump(content, launch, indent=2)


def _get_launch_path():
    return git.get_root_path() / '.vscode' / 'launch.json'


import contextlib
import json
import pathlib

import attr

from lookaside.operators import git


@attr.s
class VSCodeTask:
    name = attr.ib()
    command = attr.ib()



@attr.s
class LookasideSubcommand:
    name = attr.ib()
    subcommand = attr.ib()

    def resolve(self, lookaside_cmd):
        return VSCodeTask(self.name, f'{lookaside_cmd} {self.subcommand}')



_FILE_TASKS = [
    LookasideSubcommand('validate file nocotire', 'file-validate --no-cotire --no-unity'),
    LookasideSubcommand('validate file cotire', 'file-validate --cotire --unity'),
    LookasideSubcommand('debug file', 'vscode debug-file')
]


class FailedToParseTasksFile(Exception):
    def __init__(self, path):
        message = (
            f'Failed to parse tasks file "{path}" as JSON. '
            'Check for commants and trailing commas.'
        )
        super().__init__(self, message)
        self.message = message


def set_build_task(command):
    tasks_content = _load_tasks_file()
    tasks_content.setdefault('version', '2.0.0')
    tasks = tasks_content.setdefault('tasks', [])

    tasks = list(filter(
        lambda task: 'label' not in task or task['label'] != command.name,
        tasks
    ))

    tasks.append(_create_file_task(command))
    tasks_content['tasks'] = tasks

    _write_tasks_file(tasks_content)


def _create_file_task(command):
    return _create_task(command.name, f'{command.command} ${{file}}')


def _create_task(name, command):
    return {
        'label': name,
        'type': 'shell',
        'command': command,
        'group': 'build',
        'presentation': {
            'reveal': 'always'
        },
        'problemMatcher': []
    }


def _load_tasks_file():
    tasks_path = _get_tasks_path()

    if not tasks_path.exists():
        return {}

    with _get_tasks_path().open() as tasks:
        try:
            return json.load(tasks)
        except json.JSONDecodeError as error:
            raise FailedToParseTasksFile(tasks_path) from error


def _write_tasks_file(content):
    with _get_tasks_path().open('w') as tasks:
        json.dump(content, tasks, indent=2)


def _get_tasks_path():
    return git.get_root_path() / '.vscode' / 'tasks.json'

import click

from lookaside.cli.commands.vscode import debug
from lookaside.cli.commands.vscode import create


@click.group('vscode', help='Perform operations relevant to the Visual Studio Code editor')
def vscode():
    pass


vscode.add_command(debug.debug_file)
vscode.add_command(create.create)

import functools

import click
import pathlib

from lookaside.cli.config import decorators
from lookaside.cli.config import types
from lookaside.config import config as m_config


@click.group()
def configure():
    """Update configured values"""
    pass


@configure.group()
def defaults():
    """Update CLI default arguments"""
    pass


@defaults.group()
def cmake():
    """Update default cmake arguments"""
    pass


@cmake.command()
@click.argument('enabled', type=types.Enablable())
@decorators.update_config
def cotire(config, enabled):
    """ Set cotire default to 'yes' or 'no' """
    config.defaults.cmake.cotire = enabled



@defaults.group()
def build():
    """Update default build arguments"""
    pass


@build.command()
@click.argument('enabled', type=types.Enablable())
@decorators.update_config
def unity(config, enabled):
    """ Set unity default to 'yes' or 'no' """
    config.defaults.build.unity = enabled


@build.command()
@click.argument('enabled', type=types.Enablable())
@decorators.update_config
def clang(config, enabled):
    """ Set clang default to 'yes' or 'no' """
    config.defaults.build.clang = enabled


@build.command()
@click.argument('enabled', type=types.Enablable())
@decorators.update_config
def push(config, enabled):
    """ Set clang default to 'yes' or 'no' """
    config.defaults.build.push = enabled



@defaults.group()
def validate():
    """Update default validate arguments"""
    pass


@validate.command()
@click.argument('enabled', type=types.Enablable())
@decorators.update_config
def valgrind(config, enabled):
    """ Set valgrind default to 'yes' or 'no' """
    config.defaults.validate.valgrind = enabled


@validate.command('file-only')
@click.argument('enabled', type=types.Enablable())
@decorators.update_config
def file_only(config, enabled):
    """ Set file-only default to 'yes' or 'no' """
    config.defaults.validate.file_only = enabled


@defaults.group()
def robot():
    """Update default robot arguments"""
    pass


@robot.command()
@click.argument('enabled', type=types.Enablable())
@decorators.update_config
def report(config, enabled):
    """ Set report default to 'yes' or 'no' """
    config.defaults.robot.report = enabled


@configure.group()
def monitoring():
    """Update monitoring settings"""
    pass


@monitoring.command('enable')
@decorators.update_config
def monitoring_enable(config):
    """ Enable monitoring of errors """
    config.monitoring.enabled = True


@monitoring.command('disable')
@decorators.update_config
def monitoring_disaable(config):
    """ Disable monitoring of errors """
    config.monitoring.enabled = False


@monitoring.command()
@click.argument('url')
@decorators.update_config
def url(config, url):
    """ Set URL to which records/exceptions should be sent"""
    config.monitoring.url = url

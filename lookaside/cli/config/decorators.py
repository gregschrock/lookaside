import click
import pathlib
import functools

from lookaside.config import config as m_config


def pass_config(command):

    @click.pass_context
    def reader(ctx, *args, **kwargs):
        config = m_config.Config.create_from_file(_get_config_file())
        return ctx.invoke(command, *args, config=config, **kwargs)

    return functools.update_wrapper(reader, command)


def update_config(command):

    @pass_config
    @click.pass_context
    def configurer(ctx, config, *args, **kwargs):
        result = ctx.invoke(command, *args, config=config, **kwargs)
        config.save(_get_config_file())

        return result

    return functools.update_wrapper(configurer, command)


def _get_config_file():
    return pathlib.Path(click.get_app_dir("lookaside")) / 'config.json'
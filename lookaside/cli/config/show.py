import functools
import pathlib
import pyaml

import click

from lookaside.cli.config import decorators
from lookaside.config import config as m_config


@click.group(invoke_without_command=True)
@decorators.pass_config
@click.pass_context
def config(context, config):
    """Show configured values (executable without subcommand)"""
    context.obj = config
    if context.invoked_subcommand is None:
        _dump_to_yaml(config, m_config.ConfigSchema)


@config.group(invoke_without_command=True)
@click.pass_context
def defaults(context):
    """Show configured CLI defaults (executable without subcommand)"""
    if context.invoked_subcommand is None:
        config = context.obj
        _dump_to_yaml(config.defaults, m_config.DefaultsSchema)


@defaults.group(invoke_without_command=True)
@click.pass_context
def cmake(context):
    """Show configured CLI cmake defaults (executable without subcommand)"""
    if context.invoked_subcommand is None:
        config = context.obj
        _dump_to_yaml(config.defaults.cmake, m_config.CMakeSchema)


@cmake.command()
@click.pass_obj
def unity(config):
    _show_enablable('Cotire', config.defaults.cmake.cotire)


@defaults.group(invoke_without_command=True)
@click.pass_context
def build(context):
    """Show configured CLI build defaults (executable without subcommand)"""
    if context.invoked_subcommand is None:
        config = context.obj
        _dump_to_yaml(config.defaults.build, m_config.BuildSchema)


@build.command()
@click.pass_obj
def unity(config):
    _show_enablable('UNITY', config.defaults.build.unity)


@build.command()
@click.pass_obj
def clang(config):
    _show_enablable('Clang', config.defaults.build.clang)


@build.command()
@click.pass_obj
def push(config):
    _show_enablable('push', config.defaults.build.push)


@defaults.group(invoke_without_command=True)
@click.pass_context
def validate(context):
    """Show configured CLI validate defaults (executable without subcommand)"""
    if context.invoked_subcommand is None:
        config = context.obj
        _dump_to_yaml(config.defaults.validate, m_config.ValidateSchema)


@validate.command()
@click.pass_obj
def valgrind(config):
    _show_enablable('Valgrind', config.defaults.validate.valgrind)


@validate.command('file-only')
@click.pass_obj
def file_only(config):
    _show_enablable('File Only', config.defaults.validate.file_only)


@defaults.group(invoke_without_command=True)
@click.pass_context
def robot(context):
    """Show configured CLI robot defaults (executable without subcommand)"""
    if context.invoked_subcommand is None:
        config = context.obj
        _dump_to_yaml(config.defaults.robot, m_config.RobotSchema)


@robot.command()
@click.pass_obj
def report(config):
    _show_enablable('Report', config.defaults.robot.report)


@config.group(invoke_without_command=True)
@click.pass_context
def monitoring(context):
    """Show configured CLI defaults (executable without subcommand)"""
    if context.invoked_subcommand is None:
        config = context.obj
        _dump_to_yaml(config.monitoring, m_config.MonitoringSchema)


@monitoring.command()
@click.pass_obj
def enabled(config):
    _show_bool('Enabled', config.monitoring.enabled)


@monitoring.command()
@click.pass_obj
def url(config):
    click.echo(f'URL: {config.monitoring.url}')


def _show_enablable(name, value):
    click.echo(f'{name}: {"Enabled" if value else "Disabled"}')


def _show_bool(name, value):
    click.echo(f'{name}: {"True" if value else "False"}')


def _dump_to_yaml(obj, schema_type):
    schema = schema_type().dump(obj)
    click.echo(pyaml.dump(schema.data))
import attr
import click


class Enablable(click.Choice):
    name = 'Enable/Disable'

    def __init__(self):
        return super().__init__(['yes', 'no'])

    def convert(self, value, param, ctx):
        return super().convert(value, param, ctx) == 'yes'

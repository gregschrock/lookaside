import os
os.environ['LANG'] = 'en_US.UTF-8'

import contextlib
import functools
import json
import os.path
import pathlib
import subprocess
import sys

import attr
import click
import tabulate

import lookaside.operators.router as r_router

from lookaside.cli import monitoring
from lookaside.cli.config import configure as cli_configure
from lookaside.cli.config import decorators as config_decorators
from lookaside.cli.config import show as config_show
from lookaside.cli.commands.vscode import vscode
from lookaside.clients import ssh
from lookaside.codebase import cmake
from lookaside.codebase import file_relations
from lookaside.config import config as m_config
from lookaside.gtest import filter as gtest_filter
from lookaside.operators import compiler
from lookaside.operators import git
from lookaside.operators import tester
from lookaside.robot import robot


_CONTEXT_SETTINGS = dict(help_option_names=["-h", "--help"])


@attr.s
class Selection:
    endpoint = attr.ib(default=None)
    repo = attr.ib(default=None)


@attr.s
class CmdContext:
    selection = attr.ib(default=None)
    config = attr.ib(default=None)
    monitor = attr.ib(default=None)


@attr.s
class BuildArgs:
    targets = attr.ib()
    use_clang = attr.ib()
    use_unity = attr.ib()
    use_cotire = attr.ib()
    jobs = attr.ib()


@attr.s
class ValidateArgs:
    tests = attr.ib()
    use_valgrind = attr.ib()
    filter = attr.ib(default=None)


def _get_cwd():
    return pathlib.Path.cwd()


def _get_config_directory():
    return pathlib.Path(click.get_app_dir("lookaside"))


CONFIG = None
def _get_config():
    global CONFIG

    if CONFIG is None:
        CONFIG = m_config.Config.create_from_file(_get_config_directory() / 'config.json')

    return CONFIG


@attr.s
class SelectionContext:
    cwd = attr.ib(default=attr.Factory(_get_cwd))

    def get_name(slef):
        return 'selection'

    def get_value(self, ctx):
        if ctx.obj.selection is not None:
            return ctx.obj.selection

        try:
            return Selection(
                endpoint=_get_router().get_endpoint_for_path(self.cwd),
                repo=_get_router().get_repo_for_path(self.cwd),
            )
        except r_router.NoAssociatedRepo as e:
            click.echo(
                f'There is no repo for "{e.path}". Create one and try again.'
            )
            ctx.exit(1)
        except (r_router.NoSuchRepo, r_router.NoSuchEndpoint) as e:
            click.echo(f'{e} Create one and try again')
            ctx.exit(1)
        except r_router.NoAssociatedEndpoint as e:
            click.echo(
                f'There is no endpoint associated with "{e.repo_name}". '
                'Associate one and try again.'
            )
            ctx.exit(1)
        except r_router.InvalidAssociationException as e:
            click.echo(f'{e} Notify the maintainers.')
            ctx.exit(1)


def context_arg(cmd):
    def customizable_method(item):

        @click.pass_context
        def wrapping_cmd(ctx, *args, **kwargs):
            name = item.get_name()

            try:
                value = item.get_value(ctx)
            except Exception as e:
                click.echo(e)
                ctx.exit(1)

            kwargs[name] = value

            return ctx.invoke(cmd, *args, **kwargs)

        return functools.update_wrapper(wrapping_cmd, cmd)

    return customizable_method


def selection_command(cmd):
    return context_arg(cmd)(SelectionContext())


@click.group(context_settings=_CONTEXT_SETTINGS)
@click.pass_context
@config_decorators.pass_config
def lookaside(context, config):
    context.obj = CmdContext()
    context.obj.config = config
    context.obj.monitor = monitoring.Monitor.create(config.monitoring)


@lookaside.command('version', help='Display the current version of the tool')
def version():
    click.echo('v0.2.0')


@lookaside.command('ssh', help='SSH to the configured build endpoint')
@click.option(
    '-t', '--timeout',
    help='Seconds to wait before the connection has failed',
    default=5,
    show_default=True,
    type=click.IntRange(min=1),
)
@click.option(
    '--host', help="Don't ssh into a container in the case of a container host",
    is_flag=True,
    default=False,
)
@selection_command
def lookaside_ssh(selection, timeout, host):
    click.echo(f'Establishing SSH connection to {selection.endpoint}')

    if not selection.endpoint.dhost or host:
        cmd = '$SHELL'
    else:
        if selection.repo.active_container is not None:
            cmd = f'./tools/dsh -n {selection.repo.active_container}'
        else:
            cmd = './tools/dsh'

    try:
        print([
            'ssh',
            '-o', 'StrictHostKeyChecking=no',
            '-o', f'ConnectTimeout={timeout}',
            '-t', selection.endpoint.host,
            '-p', str(selection.endpoint.port),
            f'cd {pathlib.Path.cwd()}; {cmd}'
        ])
        subprocess.check_call([
            'ssh',
            '-o', 'StrictHostKeyChecking=no',
            '-o', f'ConnectTimeout={timeout}',
            '-t', selection.endpoint.host,
            '-p', str(selection.endpoint.port),
            f'cd {pathlib.Path.cwd()}; {cmd}'
        ])
    except subprocess.CalledProcessError as e:
        click.echo('Failed')


@lookaside.command('testbeds', help='Show information about deployed testbeds')
@click.option(
    '--verbose',
    '-v',
    help='Show the raw json as it is retrieved from tbm',
    default=False,
    show_default=True
)
@selection_command
def testbeds(selection, verbose):
    hide = not verbose

    with ssh.connect(selection.endpoint, container=selection.repo.active_container) as connection:
        try:
            user_info = json.loads(
                connection.execute_command('tbm -j query user $USER', hide=hide).stdout
            )
        except Exception:
            click.echo('Failed to retrieve user information')
            return

        try:
            testbeds = user_info['testbeds']
        except KeyError:
            click.echo(f'There were no testbeds listed for the user:\n{json.dumps(user_info, indent=2)}')

        for testbed in testbeds:
            testbed_info = json.loads(
                connection.execute_command(f'tbm -j query testbed {testbed}', hide=hide).stdout
            )

            try:
                nodes = testbed_info['nodes']
            except Exception:
                click.echo(f'{testbed} results do not contain nodes')
                continue

            node_data = [
                [node['node_name'], node['floating_ip'], node['local_ip']]
                for node in sorted(nodes, key=lambda node: node['node_name'])
            ]

            click.echo(tabulate.tabulate(
                node_data,
                headers=[testbed, 'Floating IP', 'Local IP'],
                tablefmt='simple')
            )

            click.echo('\n')


@lookaside.group('container', help='Active settings for docker containers')
def container():
    pass


@container.command('use', help='Set the active container for the docker environment')
@click.argument('name')
@selection_command
def container_use(selection, name):
    # name = f"{selection.endpoint.name}_{name}"

    if not selection.endpoint.dhost:
        click.abort(f"{selection.endpoint.name} is not configured as a docker host.")

    old_name = selection.repo.active_container

    _get_router().create_repo(
        selection.repo.name,
        selection.repo.path,
        active_container=name,
    )

    if old_name is not None:
        click.echo(f"Switched active container from '{old_name}' to '{name}'")
    else:
        click.echo(f"Set active container to '{name}''")


@container.command('clear', help='Unset the active container')
@selection_command
def container_clear(selection):
    if not selection.endpoint.dhost:
        click.abort(f"{selection.endpoint.name} is not configured as a docker host.")
    
    old_name = selection.repo.active_container

    if old_name is None:
        click.echo("There was no active container. Nothing to do.")
        return

    _get_router().create_repo(
        selection.repo.name,
        selection.repo.path,
        active_container=None
    )

    click.echo(f"Removed active container '{old_name}'")


@container.command('list', help='Unset the active container')
@selection_command
def container_list(selection):
    if not selection.endpoint.dhost:
        click.abort(f"{selection.endpoint.name} is not configured as a docker host.")

    if selection.repo.active_container is None:
        click.echo("No active container\n")
    else:
        click.echo(f"Currently using container '{selection.repo.active_container}'\n")

    with ssh.connect(selection.endpoint, dsh=False) as connection:
        connection.execute_and_print("docker ps -a")


@container.command('rm', help='Remove a container from the remote system')
@click.argument('name')
@selection_command
def container_list(selection, name):
    if not selection.endpoint.dhost:
        click.abort(f"{selection.endpoint.name} is not configured as a docker host.")

    with ssh.connect(selection.endpoint, dsh=False) as connection:
        connection.execute_command(f"docker stop {name}; docker rm {name}")


@lookaside.group('create', help='Create resources for lookaside management')
def create():
    pass


@create.command('endpoint', help='Create an endpoing on which builds/tests can run')
@click.option('--name', help='A unique, readable name for the endpoint', prompt=True)
@click.option('--host', help='The routable name or IP of the endpoint', prompt=True)
@click.option('--port', help='The port of the endpoint', prompt=True)
@click.option('--dhost/--no-dhost', help='Whether this is the host for a docker contained environment', prompt=True)
def create_endpoint(name, host, port, dhost):
    router = _get_router()
    router.create_endpoint(name, host, port, dhost)


@create.command('repo', help='Use the path as a repo to be mapped to endpoints')
@click.option('--name', help='A unique, readable name for the repo', prompt=True)
@click.option(
    '--path',
    help='The repo root path',
    type=click.Path(path_type=pathlib.Path, resolve_path=True),
    default=None,
    show_default=True
)
def create_repo(name, path):
    if path is None:
        path = _get_cwd()
    else:
        path = pathlib.Path(str(path))

    router = _get_router()
    router.create_repo(name, path)


@create.command('association', help='Set the endpoint to be used by a repo')
@click.option('--repo', help='The name of the repo', prompt=True)
@click.option('--endpoint', help='The name of the endpoint', prompt=True)
def create_association(repo, endpoint):
    router = _get_router()
    try:
        router.associate(repo, endpoint)
    except (r_router.NoSuchRepo, r_router.NoSuchEndpoint) as e:
        click.echo(f'{e} Create one and try again.')


@lookaside.group('delete', help='Delete resources from lookaside management')
def delete():
    pass


@delete.command('endpoint', help='Delete an endpoint')
@click.option('--yes', '-y', is_flag=True, default=False)
@click.argument('name')
@click.pass_context
def delete_endpoint(context, yes, name):
    router = _get_router()

    if not name in router.endpoint_names():
        click.echo(f'"{name}" is not an endpoint')
        context.exit(0)


    affected_repos = router.get_repos_referencing_endpoint(name)
    for repo in affected_repos:
        prompt = (
            f'Repo "{repo}" is associated with endpoint "{name}". '
            'Disassociate then?'
        )

        if yes or click.confirm(prompt):
            router.disassociate(repo)
        else:
            click.echo(
                f'Associations still exist for endpoint "{name}", '
                'so it will not be deleted.'
            )
            context.exit(0)

    if yes or click.confirm(f'Are you sure you want to delete {name}?'):
        router.delete_endpoint(name)
        click.echo(f'Endpoint "{name}" deleted')


@delete.command('repo', help='Delete tracking for a repo')
@click.confirmation_option(help='Are you sure you want to delete the repo?')
@click.argument('name')
@click.pass_context
def delete_repo(context, name):
    router = _get_router()

    if not name in router.repo_names():
        click.echo(f'There is no repo "{name}"')
        context.exit(1)

    with contextlib.suppress(r_router.NoAssociatedEndpoint):
        endpoint = router.get_repo_endpoint_name(name)
        router.disassociate(name)
        click.echo(f'Disassociated repo "{name}" and endpoint "{endpoint}"')

    router.delete_repo(name)
    click.echo(f'Deleted repo "{name}"')


@delete.command('association', help='Remove the association from a repo')
@click.argument('repo')
@click.pass_context
def delete_association(context, repo):
    router = _get_router()

    if not repo in router.repo_names():
        click.echo(f'There is no repo "{name}"')
        context.exit(1)

    try:
        endpoint = router.get_repo_endpoint_name(repo)
    except router.NoAssociatedEndpoint:
        click.echo(f'There are no associations for repo "{repo}"')
    else:
        prompt = (
            'Are you sure you want to disassociate repo '
            f'"{repo}" and endpoint "{endpoint}"'
        )

        if click.confirm(prompt):
            router.disassociate(repo)
            click.echo(f'Disassociated repo "{repo}" and endpoint "{endpoint}"')


@lookaside.group('show', help='Display resource information')
def show():
    pass


@show.command('repos', help='Show the repos that exist')
def show_repos():
    router = _get_router()
    table_data = [[repo.name, repo.path, repo.active_container] for repo in router.repos()]

    click.echo(tabulate.tabulate(
        table_data,
        headers=['Name', 'Path', 'Active Container'],
        tablefmt='simple')
    )


@show.command('endpoints', help='Show the endpoints that exist')
def show_endpoints():
    router = _get_router()
    table_date = [
        [endpoint.name, endpoint.host, endpoint.port, endpoint.dhost]
        for endpoint in router.endpoints()
    ]

    click.echo(tabulate.tabulate(
        table_date,
        headers=['Name', 'Host', 'Port', 'Docker Host'],
        tablefmt='simple'
    ))


@show.command('associations', help='Show repo/endpoint associations')
def show_associations():
    router = _get_router()
    table_data = [
        [association.repo, association.endpoint]
        for association in router.associations()
    ]

    click.echo(tabulate.tabulate(
        table_data,
        headers=['Repo', 'Endpoint'],
        tablefmt='simple'
    ))


@lookaside.command('describe', help='Show codebase information about the provided file')
@click.argument('path', type=click.Path(exists=True))
def describe(path):
    path = pathlib.Path(path).resolve()

    table_data = [['File', str(path)]]
    with contextlib.suppress(FileNotFoundError):
        table_data.append(
            ['Unit Test', file_relations.get_unit_test_path(path)])

    with contextlib.suppress(FileNotFoundError):
        table_data.append(['Target', cmake.Target.create_for_file(path).name])

    with contextlib.suppress(FileNotFoundError):
        table_data.append(
            ['Test Target', cmake.TestTarget.create_for_file(path).name])

    click.echo(tabulate.tabulate(
        table_data,
        tablefmt='simple'
    ))


@lookaside.command('run', help='Run a custom command on the remote machine')
@selection_command
@click.argument('command')
def lookaside_run(selection, command):
    sys.exit(_run(selection.endpoint, command))


@lookaside.command(
    'clean-all',
    help='Run download_wheels and cleanWeb on the endpoint'
)
@selection_command
def lookaside_clean(selection):
    sys.exit(_run(selection.endpoint, 'download_wheels; cleanWeb'))


@lookaside.command(
    'robot',
    help='Run robot from the endpoint'
)
@selection_command
@click.option(
    '--run-type',
    type=click.Choice(robot.get_run_options()),
    required=True
)
@click.option(
    '--build-type',
    type=click.Choice(robot.get_build_options()),
    default=robot.get_build_options()[0]
)
@click.option(
    '--testbed', '-t',
    help='The testbed to run robot on',
    type=int,
    required=True
)
@click.option(
    '--suite', '-s',
    help='The test suite to run',
    type=click.Path(exists=True),
    required=True
)
@click.option(
    '--report/--no-report',
    help='Retrieve and open the robot run report',
    default=_get_config().defaults.robot.report,
    show_default=True
)
@click.option(
    '--robot-variable', '-v',
    help='Robot variable',
    multiple=True
)
@click.option(
    "--randomize/--no-randomize",
    help='Whether or not to randomize the tests',
    default=False,
    is_flag=True,
    show_default=True,
)
@click.option(
    "--extra",
    "extras",
    help="Extra arguments to send to run_robot - e.g. (--extra -i stable)",
    type=(str, str),
    multiple=True,
)
def lookaside_robot(selection, run_type, build_type, testbed, suite, report, robot_variable, randomize, extras):
    suite = pathlib.Path(suite).resolve()
    remote_out = os.path.expanduser(f"~/.lookaside/robot/{_get_repo().name}")
    local_out = "./release"

    with ssh.connect(selection.endpoint, container=_get_repo().active_container) as connection:
        connection.set_directory_to_cwd()

        result = robot.run(
            connection,
            run_type=run_type,
            build_type=build_type,
            testbed=testbed,
            suite=pathlib.Path(suite),
            robot_variables=robot_variable,
            outdir=remote_out,
            randomize=randomize,
            extras=extras,
        )

        if report:
            robot.retrieve_results(connection, remote_out, local_out)
            robot.open_results(local_out, suite)

        sys.exit(result.return_code)


@lookaside.command(
    'robot-report',
    help='Report the results of a robot run'
)
@selection_command
@click.option(
    '--suite', '-s',
    help='The test suite to run',
    type=click.Path(exists=True),
    required=True
)
@click.option(
    '--outdir',
    help='The directory that will contain the results',
    type=click.Path(),
    default='./release'
)
def lookaside_robot_report(selection, suite, outdir):
    suite = pathlib.Path(suite).resolve()
    remote_out = os.path.expanduser(f"~/.lookaside/robot/{_get_repo().name}")

    with ssh.connect(selection.endpoint, container=selection.repo.active_container) as connection:
        connection.set_directory_to_cwd()

        robot.retrieve_results(connection, remote_out, outdir)
        robot.open_results(outdir, suite)


@lookaside.command('push', help='Push the current changes to the endpoint')
@selection_command
def push(selection):
    _push(selection.endpoint, selection.repo)


@lookaside.command('build', help='Build one or more targets')
@selection_command
@click.argument('target_list', 'target', nargs=-1)
@click.option(
    '--clang', '-c',
    is_flag=True,
    help='Build with clang (gcc by default)',
    default=_get_config().defaults.build.clang
)
@click.option(
    '--unity/--no-unity',
    default=_get_config().defaults.build.unity,
    show_default=True
)
@click.option(
    '--cotire/--no-cotire',
    default=_get_config().defaults.cmake.cotire,
    show_default=True
)
@click.option(
    '--push/--no-push',
    default=_get_config().defaults.build.push,
    show_default=True
)
@click.option("--jobs", "-j", type=click.IntRange(min=1))
def lookaside_build(selection, target_list, clang, unity, cotire, push, jobs):
    build_args = BuildArgs(target_list, clang, unity, cotire, jobs)

    if push:
        _push(selection.endpoint, selection.repo)
    sys.exit(_build(selection.endpoint, build_args))


@lookaside.command('validate', help='Test one or more targets')
@selection_command
@click.argument('target_list', 'target', nargs=-1)
@click.option(
    '--clang', '-c',
    is_flag=True,
    help='Build with clang (gcc by default)',
    default=_get_config().defaults.build.clang
)
@click.option(
    '--unity/--no-unity',
    default=_get_config().defaults.build.unity,
    show_default=True
 )
@click.option(
    '--cotire/--no-cotire',
    default=_get_config().defaults.cmake.cotire,
    show_default=True
)
@click.option('--build/--skip-build', default=True, show_default=True)
@click.option('-f', help='Filter string', default=None, show_default=True)
@click.option(
    '--valgrind/--no-valgrind',
    default=_get_config().defaults.validate.valgrind,
    show_default=True
)
@click.option(
    '--push/--no-push',
    default=_get_config().defaults.build.push,
    show_default=True
)
@click.option("--jobs", "-j", type=click.IntRange(min=1))
def lookaside_validate(selection, target_list, clang, unity, cotire, f, build, valgrind, push, jobs):
    build_args = BuildArgs(target_list, clang, unity, cotire, jobs)
    validate_args = ValidateArgs(target_list, valgrind, f)

    sys.exit(_build_and_validate(selection.endpoint, selection.repo, push, build, build_args, validate_args))


@lookaside.command('file-validate', help='Run the tests associated with the specified file')
@selection_command
@click.argument('path', 'target-file', type=click.Path(exists=True))
@click.option('-c', is_flag=True, help='Build with clang (gcc by default)', default=False)
@click.option(
    '--unity/--no-unity',
    default=_get_config().defaults.build.unity,
    show_default=True
)
@click.option(
    '--cotire/--no-cotire',
    default=_get_config().defaults.cmake.cotire,
    show_default=True
)
@click.option('--build/--skip-build', default=True, show_default=True)
@click.option('-f', help='Filter string', default=None, show_default=True)
@click.option(
    '--file-only/--all',
    help="Filter to tests within this file's unit test (overriden by -f)",
    default=_get_config().defaults.validate.file_only,
    show_default=True
)
@click.option(
    '--valgrind/--no-valgrind',
    default=_get_config().defaults.validate.valgrind,
    show_default=True
)
@click.option(
    '--push/--no-push',
    default=_get_config().defaults.build.push,
    show_default=True
)
@click.option("--jobs", "-j", type=click.IntRange(min=1))
def lookaside_file_validate(selection, path, c, unity, cotire, f, file_only, build, valgrind, push, jobs):
    path = pathlib.Path(path)

    targets = [cmake.TestTarget.create_for_file(path).name]

    if file_only and f is None:
        unit_test = file_relations.get_unit_test_path(path)
        f = gtest_filter.get_filters_for_tests_in(unit_test)

    build_args = BuildArgs(targets, c, unity, cotire, jobs)
    validate_args = ValidateArgs(targets, valgrind, f)

    sys.exit(_build_and_validate(selection.endpoint, selection.repo, push, build, build_args, validate_args))


lookaside.add_command(cli_configure.configure)
lookaside.add_command(vscode.vscode)

show.add_command(config_show.config)




def _push(endpoint, repo):
    click.echo(f'Pushing to {endpoint}...')
    git.push_to_lookaside(endpoint, repo)


def _build_and_validate(endpoint, repo, push, do_build, build_args, validate_args):
    if do_build:
        if push:
            _push(endpoint, repo)
        build_code = _build(endpoint, build_args)

        if build_code == 0:
            return _validate(endpoint, validate_args)
        else:
            return build_code
    else:
        return _validate(endpoint, validate_args)


def _build(endpoint, build_args):
    click.echo(build_args.targets)
    build_targets = cmake.Targets.create_from_list(build_args.targets)

    flags = compiler.Flags(
        use_clang=build_args.use_clang,
        use_unity=build_args.use_unity,
        use_cotire=build_args.use_cotire,
        jobs=build_args.jobs,
    )

    with ssh.connect(endpoint, container=_get_repo().active_container) as connection:
        connection.set_directory_to_cwd()

        comp = compiler.Compiler(
            connection,
            flags=flags,
        )

        return comp.compile(build_targets).return_code


def _validate(endpoint, validate_args):
    with ssh.connect(endpoint, container=_get_repo().active_container) as connection:
        connection.set_directory_to_cwd()
        return tester.run_targets(
            connection,
            validate_args.tests,
            valgrind=validate_args.use_valgrind,
            filter=validate_args.filter
        ).return_code


def _run(endpoint, command):
    with ssh.connect(endpoint, container=_get_repo().active_container) as connection:
        connection.set_directory_to_cwd()
        return connection.execute_and_print(command).return_code


def _get_router():
    config_directory = _get_config_directory()
    router = r_router.Router(config_directory)
    router._load_resources()

    return router


def _get_repo():
    return _get_router().get_repo_for_path(_get_cwd())


if __name__ == '__main__':
    lookaside()

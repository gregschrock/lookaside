import attr
import sentry_sdk


@attr.s
class Monitor:
    is_enabled = attr.ib(default=True)

    @classmethod
    def create(cls, monitoring_config):
        if monitoring_config.enabled:
            try:
                sentry_sdk.init(monitoring_config.url, release='lookaside@0.1.0')
            except:
                print('Disabling monitoring because of initialization failure')
                monitoring_config.enabled = False

        return cls(monitoring_config.enabled)

    def trail(self, message):
        if self.is_enabled:
            sentry_sdk.add_breadcrumb(level='info', message=message)

    def report(self, exception):
        if self.is_enabled:
            sentry_sdk.capture_exception(exception)
    
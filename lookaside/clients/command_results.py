#!/usr/local/bin/python3

import attr
import click


@attr.s
class PipeIter:
    my_pipe = attr.ib()
    _my_index = 0

    def __next__(self):
        if self.my_pipe.eof:
            raise StopIteration

        if len(self.my_pipe) <= self._my_index:
            result = self.my_pipe.readline()
        else:
            result = self.my_pipe.lines[self._my_index]

        self._my_index += 1
        return result


@attr.s
class Pipe:
    pipe = attr.ib()
    lines = list()
    eof = False

    def __len__(self):
        return len(self.lines)

    def __iter__(self):
        return PipeIter(self)

    @property
    def full_output(self):
        while not self.eof:
            self.readline()
        return self.lines

    def has_output(self):
        return len(self) > 0 or self.readline()

    def readline(self):
        line = self.pipe.readline()

        if not line:
            self.eof = True
        else:
            self.lines.append(line)

        return line

    def display(self):
        for line in self:
            # click.secho(line, nl=False)
            click.echo(line, nl=False)


@attr.s
class CommandResults:
    command = attr.ib()
    stdout = attr.ib()
    stderr = attr.ib()

    @classmethod
    def create(cls, *, command, stdout, stderr):
        return cls(command=command, stdout=Pipe(stdout), stderr=Pipe(stderr))

    def display(self):
        self.stdout.display()
        self.stderr.display()

    def has_output(self):
        return self.stdout.has_output()

    def has_error(self):
        return self.stderr.has_output()

    def was_successful(self):
        return self.exit_status() == 0

    def wait(self):
        self.stdout.full_output

    def raise_on_failure(self):
        if not self.was_successful():
            raise CommandFailed.create(
                command=self.command,
                return_code=self.exit_status(),
                stdout=self.stdout.full_output,
                stderr=self.stderr.full_output
            )

    def exit_status(self):
        return self.stdout.pipe.channel.recv_exit_status()

    def __str__(self):
        return f'OUTPUT: {self.stdout.full_output}\nERROR: {self.stderr.full_output}'


@attr.s
class CommandFailed(Exception):
    command = attr.ib()
    return_code = attr.ib()
    stdout = attr.ib()
    stderr = attr.ib()

    @classmethod
    def create(cls, *, command, return_code, stdout, stderr):
        return cls(
            command=command,
            return_code=return_code,
            stdout=stdout,
            stderr=stderr
        )

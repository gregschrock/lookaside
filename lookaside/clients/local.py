#!/usr/local/bin/python3

import pathlib

import attr

from lookaside.clients import ssh
from lookaside.operators import resources

# import subprocess


# from lookaside.clients import command_results


@attr.s
class Client:
    _connection = attr.ib()
    cwd = attr.ib(default='~')

    @classmethod
    def create(cls, *, cwd='~'):
        connection = ssh.Client.create(
            endpoint=resources.Endpoint.create_by_name(
                name='localhost',
                hostname='localhost'
            ),
            auth=ssh.Auth.from_keys()
        )

        return cls(connection, cwd)

    def execute_and_print(self, command):
        self._connection.set_directory(self.cwd)
        return self._connection.execute_and_print(command)

    def execute_command(self, command):
        self._connection.set_directory(self.cwd)
        return self._connection.execute_and_print(command)

    def set_directory(self, directory):
        self.cwd = directory

    def set_directory_to_cwd(self):
        self.cwd = pathlib.Path.cwd()

    # def execute_and_print(self, command):
    #     results = self.execute_command(command)
    #     results.display()

    #     return results

    # def execute_command(self, command):
    #     cmd = f'cd {self.cwd}; {command}'

    #     proc = subprocess.Popen(
    #         cmd,
    #         shell=True,
    #         stdout=subprocess.PIPE,
    #         stderr=subprocess.PIPE,
    #         universal_newlines=True
    #     )

    #     results = command_results.CommandResults.create(
    #         command=command,
    #         stdout=proc.stdout,
    #         stderr=proc.stderr
    #     )

    #     return results

    # def set_directory(self, directory):
    #     self.cwd = directory

    # def set_directory_to_cwd(self):
    #     self.cwd = pathlib.Path.cwd()

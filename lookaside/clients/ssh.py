#!/usr/local/bin/python3

import contextlib
import pathlib
import shutil
import subprocess
import warnings

import attr
# import paramiko

import cryptography.utils
import fabric
import invoke

from lookaside.clients import command_results
# from lookaside import constants


@attr.s
class Auth:
    connect_kwargs = attr.ib(default=attr.Factory(dict))

    @classmethod
    def from_password(cls, *, username, password):
        return cls({'username': username, 'password': password})

    @classmethod
    def from_keys(cls):
        return cls()


@contextlib.contextmanager
def connect(endpoint, auth=Auth.from_keys(), dsh=True, container=None):
    client = Client.create(
        endpoint=endpoint,
        auth=auth,
        dsh=dsh,
        container=container,
    )

    yield client

    client.close()


@attr.s
class Client:
    endpoint = attr.ib()
    ssh_connection = attr.ib()
    cwd = attr.ib(default='~')
    dsh = attr.ib(default=True)
    container = attr.ib(default=None)

    @classmethod
    def create(cls, *, endpoint, auth, dsh=True, container=None):
        connection = fabric.Connection(
            host=endpoint.host,
            port=endpoint.port,
            connect_kwargs=auth.connect_kwargs,
        )

        return cls(endpoint, connection, dsh=dsh, container=container)


    def execute_and_print(self, command):
        return self._execute(command)

    def execute_command(self, command, hide=False):
        return self._execute(command, hide=hide)

    def get_dir(self, remote, local):
        destination = pathlib.Path(local).resolve()
        shutil.rmtree(local, ignore_errors=True)

        source = f'{self.endpoint.host}:{remote}'

        return subprocess.run([
            'scp', '-r', '-P',  str(self.endpoint.port),
            source,
            destination
        ])

        # TODO: Consider using the fabric.transfer functionality (didn't work previously)
        # transfer = fabric.transfer.Transfer(self.ssh_connection)
        # return transfer.get(remote, local)

    def _execute(self, command, hide=False):
        if self.endpoint.dhost and self.dsh:
            if self.container is not None:
                # name = f'-n {self.container} -s {constants.DSH_PORT} '
                name = f'-n {self.container} '
            else:
                name = ''

            cmd = f"cd {self.cwd}; ./tools/denv {name}-- '{command}'"
        else:
            cmd = f'cd {self.cwd}; {command}'


        try:
            print(cmd)
            with _ignore_crypto_warnings():
                return self.ssh_connection.run(cmd, hide=hide, pty=True)
        except invoke.UnexpectedExit as e:
            return e.result

    def set_directory(self, directory):
        self.cwd = directory

    def set_directory_to_cwd(self):
        self.cwd = pathlib.Path.cwd()

    def close(self):
        self.ssh_connection.close()


@contextlib.contextmanager
def _ignore_crypto_warnings():
    with warnings.catch_warnings():
        warnings.simplefilter(
            'ignore',
            cryptography.utils.CryptographyDeprecationWarning
        )

        yield

import enum
import re

import attr

from lookaside.codebase import cmake
from lookaside.codebase import file_relations


class TargetNotFoundError(Exception):
    def __init__(self, path, searched_cmake_files):
        searched_list = '\n\t'.join([
            str(cmake_file) for cmake_file in searched_cmake_files
        ])

        super().__init__(
            f'Target not found for {path}\n\n'
            f'  Searched in [\n{searched_cmake_files}\n]'
        )

        self.path = path
        self.searched_files = searched_cmake_files


class NoSuchTargetError(Exception):
    def __init__(self, path, target_name):
        super().__init__(
            f'The file "{path}" does not contain a target {target_name}'
        )


@attr.s
class Targets:
    targets = attr.ib(default=attr.Factory(list))

    @classmethod
    def create_from_list(cls, targets):
        return cls([Target(target) for target in targets])

    def as_unity(self, unity=True):
        return [target.unity for target in self.targets]

    def as_non_unity(self):
        return [target.non_unity for target in self.targets]


class TargetTypes(enum.Enum):
    UNSPECIFIED = 0
    EXECUTABLE = 1
    LIBRARY = 2


@attr.s
class Target:
    NEVER_UNITY = ['cpp', 'product', 'web', 'rpm']

    name = attr.ib()
    directory = attr.ib(default=None)
    _type = attr.ib(default=TargetTypes.UNSPECIFIED)

    @classmethod
    def create_for_file(cls, path):
        path = path.resolve()
        path = path.with_suffix('.cpp')

        if not path.exists():
            raise FileNotFoundError(path)

        cmake_lists = cmake.CMakeLists.create_from_directory(path.parent)
        files_searched = []

        while cmake_lists.get_parent_cmake().path != cmake_lists.path:
            if cmake_lists.exists():
                files_searched.append(cmake_lists.path)
                cmake_lists.load_contents()

                if cmake_lists.contains_target(path.name):
                    return cmake_lists.get_target(path.name)

            cmake_lists = cmake_lists.get_parent_cmake()

        raise TargetNotFoundError(path, files_searched)

    @property
    def unity(self):
        if self.name in self.NEVER_UNITY:
            return self.name
        else:
            return self.name + '_unity'

    @property
    def non_unity(self):
        return self.name

    @property
    def is_executable(self):
        return self._type == TargetTypes.EXECUTABLE

    @property
    def is_library(self):
        return self._type == TargetType.LIBRARY


@attr.s
class TestTarget(Target):

    @classmethod
    def create_for_file(cls, path):
        path = path.resolve()

        if not path.exists():
            raise FileNotFoundError(path)

        unit_test = file_relations.get_unit_test_path(path)

        if not unit_test.exists():
            raise FileNotFoundError(unit_test)

        return Target.create_for_file(unit_test)

@attr.s
class CMakeLists:
    path = attr.ib()
    file_lines = attr.ib(default=None)

    @classmethod
    def create_from_directory(cls, directory):
        return CMakeLists(directory / 'CMakeLists.txt')

    def exists(self):
        return self.path.exists()

    def get_parent_cmake(self):
        return CMakeLists.create_from_directory(self.path.parent.parent)

    def load_contents(self):
        with self.path.open('r') as cmake_lines:
            self.file_lines = list(cmake_lines)

    def get_target(self, name):
        assert(self.file_lines is not None)

        target_indecies = _get_indecies_containing(self.file_lines, name)

        if len(target_indecies) == 0:
            raise NoSuchTargetError(self.path, name)

        target_index = target_indecies[0]

        for index in range(target_index, -1, -1):
            line = self.file_lines[index]

            if _line_contains_executable(line):
                return _get_executable_target_from_line(line, self.path)
            if _line_contains_library(line):
                return _get_library_target_from_line(line, self.path)

        raise NoSuchTargetError(self.path, name)

    def contains_target(self, name):
        assert(self.file_lines is not None)

        return len(_get_indecies_containing(self.file_lines, name)) > 0


def _get_indecies_containing(lines, contents):
    return [index for index, line in enumerate(lines) if contents in line]


def _line_contains_library(line):
    return 'add_library_128' in line


def _line_contains_executable(line):
    return 'add_executable_128' in line


def _get_executable_target_from_line(line, cmake_path):
    regex = 'add_executable_128\((.*?)$'
    match = re.search(regex, line)
    return Target(
        match.group(1),
        directory=cmake_path.parent,
        type=TargetTypes.EXECUTABLE
    )


def _get_library_target_from_line(line, cmake_path):
    regex = 'add_library_128\(\$\{PROJECT_NAME\}_(.*?) (?:SHARED|STATIC)$'
    match = re.search(regex, line)
    return Target(
        '128T_' + match.group(1),
        directory=cmake_path.parent,
        type=TargetTypes.LIBRARY
    )
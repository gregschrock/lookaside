#!/usr/local/bin/python3

import os


def get_file_contents(file_path):
    contents = None

    file_path = file_path.resolve()

    if not file_path.exists():
        print('Cannot find file: ' + file_path)
    else:
        with file_path.open('r') as f:
            contents = f.read()

    return contents

#!/usr/local/bin/python3

import os

from lookaside.operators import git


def is_test(file_path):
    return str(file_path).endswith('Test.cpp')


def is_test_directory(directory_path):
    return directory_path.name == 'test'


def is_in_test_directory(file_path):
    return is_test_directory(_get_directory(file_path))


def get_unit_test_path(file_path):
    test_directory = get_unit_test_directory(file_path)
    test_name = _get_unit_test_name(file_path)
    unit_test_path =  test_directory / test_name

    if not unit_test_path.exists():
        raise FileNotFoundError(unit_test_path)
    
    return unit_test_path


def get_unit_test_directory(file_path):
    directory = _get_directory(file_path)
    return directory if is_in_test_directory(file_path) or is_test(file_path) else directory / 'test'


def get_built_executable_path(executable_path):
    root_directory = git.get_root_path()
    relative_path = executable_path.relative_to(root_directory)

    return root_directory / 'build' / relative_path


def _get_unit_test_name(file_path):
    file_name = file_path.name
    return file_name if is_test(file_path) else file_name[:-4] + 'Test.cpp'


def _get_directory(file_path):
    return file_path if file_path.is_dir() else file_path.parent




# def get_cmake_path(directory_path):
#     cmake_file = os.path.join(directory_path, 'CMakeLists.txt')
#     return cmake_file if os.path.exists(cmake_file) else None


# def get_integration_test_directory(file_path):
#     def append_integration_directory(directory):
#         return os.path.join(directory, 'integrationTest')

#     def contains_integration_directory(directory):
#         return file_exists(append_integration_directory(directory))

#     directory = find_ancestor_directory_where(
#         file_path, contains_integration_directory)

#     return None if directory is None else append_integration_directory(directory)


# def find_ancestor_directory_where(file_path, meets_condition, stop_at_directory='/'):
#     directory = _get_directory(file_path)

#     if directory == stop_at_directory:
#         return None

#     if meets_condition(directory):
#         return directory
#     else:
#         return find_ancestor_directory_where(directory, meets_condition, stop_at_directory)

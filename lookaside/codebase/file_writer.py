#!/usr/local/bin/python3

import os


def write_file(file_path, file_contents):
    file_path.parent.mkdir(parents=True, exist_ok=True)

    with file_path.open('w') as f:
        f.write(file_contents)

import json

import attr
import marshmallow


#---------------------------------------------------
# Steps to add config
#
# 1. Add value to the Config or child objects
# 2. Update/create the relevant schema
# 3. Add a show config command
# 4. Add a configure command
#---------------------------------------------------


@attr.s
class CMake:
    cotire = attr.ib(default=True)


@attr.s
class Build:
    unity = attr.ib(default=False)
    clang = attr.ib(default=False)
    push = attr.ib(default=True)


@attr.s
class Validate:
    valgrind = attr.ib(default=False)
    file_only = attr.ib(default=False)


@attr.s
class Robot:
    report = attr.ib(default=True)


@attr.s
class Defaults:
    cmake = attr.ib(default=attr.Factory(CMake))
    build = attr.ib(default=attr.Factory(Build))
    validate = attr.ib(default=attr.Factory(Validate))
    robot = attr.ib(default=attr.Factory(Robot))


@attr.s
class Monitoring:
    enabled = attr.ib(default=False)
    url = attr.ib(default=None)


@attr.s
class Config:
    defaults = attr.ib(default=attr.Factory(Defaults))
    monitoring = attr.ib(default=attr.Factory(Monitoring))

    @classmethod
    def create_from_file(cls, path):
        if not path.exists():
            return cls()

        with path.open('r') as data_file:
            json_data = json.load(data_file)

            return ConfigSchema().load(json_data).data

    def save(self, path):
        data_json = ConfigSchema().dump(self).data

        with path.open('w') as data_file:
            json.dump(data_json, data_file)



class CMakeSchema(marshmallow.Schema):
    cotire = marshmallow.fields.Boolean()

    @marshmallow.post_load
    def create(self, data):
        return CMake(**data)



class BuildSchema(marshmallow.Schema):
    unity = marshmallow.fields.Boolean()
    clang = marshmallow.fields.Boolean()
    push = marshmallow.fields.Boolean()

    @marshmallow.post_load
    def create(self, data):
        return Build(**data)


class ValidateSchema(marshmallow.Schema):
    valgrind = marshmallow.fields.Boolean()
    file_only = marshmallow.fields.Boolean()

    @marshmallow.post_load
    def create(self, data):
        return Validate(**data)


class RobotSchema(marshmallow.Schema):
    report = marshmallow.fields.Boolean()

    @marshmallow.post_load
    def create(self, data):
        return Robot(**data)


class DefaultsSchema(marshmallow.Schema):
    cmake = marshmallow.fields.Nested(CMakeSchema())
    build = marshmallow.fields.Nested(BuildSchema())
    validate = marshmallow.fields.Nested(ValidateSchema())
    robot = marshmallow.fields.Nested(RobotSchema())

    @marshmallow.post_load
    def create(self, data):
        return Defaults(**data)


class MonitoringSchema(marshmallow.Schema):
    enabled = marshmallow.fields.Boolean()
    url = marshmallow.fields.Str(required=True, allow_none=True)

    @marshmallow.post_load
    def create(self, data):
        return Monitoring(**data)


class ConfigSchema(marshmallow.Schema):
    defaults = marshmallow.fields.Nested(DefaultsSchema())
    monitoring = marshmallow.fields.Nested(MonitoringSchema())

    @marshmallow.post_load
    def create(self, data):
        return Config(**data)

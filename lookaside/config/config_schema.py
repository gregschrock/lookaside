import marshmallow

from lookaside.config import config

class Build(marshmallow.Schema):
    valgrind = marshmallow.fields.Boolean()

    @arshmallow.post_load
    def create(self, data):
        return config.Build(**data)


class Config(marshmallow.Schema):
    build = marshmallow.fields.Nested(Build())

    @arshmallow.post_load
    def create(self, data):
        return config.Config(**data)
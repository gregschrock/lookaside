#!/usr/bin/python2

import argparse
import commands
import file_reader
import git
import notify

import csv
import json


def add_csv_row(row_label, csv_file, value_dict):
    existing_columns = get_existing_document_columns(csv_file)
    values = [value_dict[key] for key in sorted(value_dict.keys())]

    if existing_columns == None:
        write_csv_header(csv_file, value_dict)
    else:
        ensure_columns_match(existing_columns, value_dict)

    append_csv_row(csv_file, row_label, values)


def write_csv_header(csv_file, value_dict):
    append_csv_row(csv_file, '', sorted(value_dict.keys()))


def append_csv_row(csv_file, row_label, values):
    with open(csv_file, 'a') as out_file:
        csv_writer = csv.writer(out_file)
        csv_writer.writerow([row_label] + values)


def ensure_columns_match(existing_columns, value_dict):
    if (sorted(existing_columns) != sorted(value_dict.keys())):
        print('expected: ' + str(sorted(existing_columns)) +
              '\nbut got:' + str(sorted(value_dict.keys())))
        assert(False)


def get_existing_document_columns(csv_file):
    file_content = file_reader.get_file_contents(csv_file)
    if file_content == None or len(file_content) == 0:
        return None
    else:
        return file_content.splitlines()[0].split(',')[1:]


def get_dict_from_key_string(key_string):
    pairs = key_string.split(',')
    key_value_pairs = [pair.split(':') for pair in pairs]
    value_dict = {}

    for pair in key_value_pairs:
        key = pair[0].strip()
        value = pair[1].strip()
        value_dict[key] = value

    return value_dict


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Add csv row to existing csv file')

    parser.add_argument('csv_file', action='store',
                        help='The csv file to append')

    parser.add_argument('new_row_label', action='store',
                        help='The label to give the newly added row')

    parser.add_argument('value_dict', action='store',
                        help='The key value paris for the row (e.g. value_dict="column1: value1, column2: value2"')

    args = parser.parse_args()
    value_dict = get_dict_from_key_string(args.value_dict)
    print(value_dict)

    add_csv_row(args.new_row_label, args.csv_file, value_dict)

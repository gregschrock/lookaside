#!/usr/local/bin/python3

import argparse
import file_reader

import os


class FileCommand:
    def __init__(self, description):
        parser = argparse.ArgumentParser(description=description)
        parser.add_argument('target', help='target file path')
        args = parser.parse_args()

        self.file_path = os.path.abspath(args.target)

    def file_exists(self):
        return os.path.exists(self.file_path)

    def get_file_path(self):
        return self.file_path

    def get_file_contents(self):
        return file_reader.get_file_contents(self.file_path)

    def fail_if_not_exists(self):
        if not self.file_exists():
            print('Cannot find targets for nonexistent file: ' + self.file_path)
            exit(-1)

#!/usr/local/bin/python3

import click
import git
import subprocess

_FILTER_UNIT = '*/test/*'
_FILTER_INTEGRATION = '*/integrationTest/*'
_FILTER_APPLICATIONS = '*/apps/*'

"find . -name 'CMakeLists.txt' - path '*/test/*' - o - path '*/integrationTest/*' - print0 | xargs - 0 grep - h 'add_executable_128' | awk '{ princt $1 }'"


@click.command('executables')
@click.option('-i', '--integration-tests', is_flag=True, help='Include integration test executables')
@click.option('-u', '--unit-tests', is_flag=True, help='Include unit test executables')
@click.option('-a', '--applications', is_flag=True, help='Include application executables')
def list_executables(integration_tests, unit_tests, applications):
    get_all = (integration_tests == unit_tests == applications)

    include, exclude = FilterListGenerator(
        integration_tests, unit_tests, applications).get_filters()

    executables = get_executables_with_path_filters(include, exclude)
    for executable in executables:
        print(executable)


class FilterListGenerator:
    def __init__(self, integration_tests, unit_tests, applications):
        self.include_paths = []
        self.exclude_paths = []

        self.get_all = integration_tests and unit_tests and applications

        self.add_filter_for(integration_tests, _FILTER_INTEGRATION)
        self.add_filter_for(unit_tests, _FILTER_UNIT)
        self.add_filter_for(applications, _FILTER_APPLICATIONS)

    def get_filters(self):
        return self.include_paths, self.exclude_paths

    def add_filter_for(self, was_flagged, filter, explicit=False):
        if was_flagged or (self.get_all and not explicit):
            self.include_paths.append(filter)
        else:
            self.exclude_paths.append(filter)


def get_executables_with_path_filters(include_paths, exclude_paths):
    root = git.get_root_path()
    include_string = build_include_paths(include_paths)
    exclude_string = build_exclude_paths(exclude_paths)

    cmd = ("find {!r} -name 'CMakeLists.txt' {} {} -print0 | "
           "xargs -0 grep -h 'add_executable_128' | "
           "awk '{{ print $1 }}'").format(root, include_string, exclude_string)

    return [line.decode('utf-8').split('(')[1]
            for line in subprocess.check_output(cmd, shell=True).splitlines()]


def build_include_paths(include_paths):
    return ' -o '.join(['-path {!r}'.format(path) for path in include_paths])


def build_exclude_paths(exclude_paths):
    return ' '.join(['-not -path {!r}'.format(path) for path in exclude_paths])


if __name__ == '__main__':
    list_executables()

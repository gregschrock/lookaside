import re

from lookaside.codebase import file_reader


def get_filters_for_tests_in(file_path):
    with file_path.open('r') as stream:
        file_content = stream.read()

    return _get_filters_for_test_in_content(file_content)


def _get_filters_for_test_in_content(file_content):
    test_matchers = _generate_matchers(
        _get_filter_pairs_from_content(file_content))
    return ':'.join(test_matchers)


def _get_filter_pairs_from_content(file_content):
    test_pairs = []
    test_pairs.extend(_get_simple_test_pairs(file_content))
    test_pairs.extend(_get_fixture_test_pairs(file_content))
    test_pairs.extend(_get_paramatrized_test_pairs(file_content))

    return test_pairs


def _generate_matchers(test_pairs):
    stripped_pairs = _get_stripped_pairs(test_pairs)
    test_names = ['.'.join(pair) for pair in stripped_pairs]
    return ['*' + name + '*' for name in test_names]


def _get_stripped_pairs(test_pairs):
    return [[pair[0].strip(), pair[1].strip()] for pair in test_pairs]


def _get_simple_test_pairs(file_content):
    return _get_test_name_pairs(file_content, 'TEST')


def _get_fixture_test_pairs(file_content):
    return _get_test_name_pairs(file_content, 'TEST_F')


def _get_paramatrized_test_pairs(file_content):
    return _get_test_name_pairs(file_content, 'TEST_P')


def _get_test_name_pairs(file_content, test_type_str):
    search_string = test_type_str + '\((.*?),(.*?)\)'
    return re.findall(search_string, file_content)

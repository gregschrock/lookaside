#!/usr/local/bin/python3

import command
# import pync
# from pync import TerminalNotifier

__REQUIRE_ACTIONS__ = ['open']


def notify(message, **kwargs):
    arguments = _populate_with_defaults(kwargs)
    cmd = _format_command(message, arguments)
    command.Command.create(cmd).result.display()

    # pync.Notifier.notify(message, **kwargs)


def _populate_with_defaults(arguments):
    arguments.setdefault('sound', 'Glass')

    if (_has_action(arguments)):
        arguments.setdefault('action', '')

    return arguments


def _format_command(message, arguments):
    formated_arguments = [
        '-{} {!r}'.format(key, arguments[key]) for key in arguments]

    arguments_string = ' '.join(formated_arguments)

    return 'terminal-notifier -message {!r} {}'.format(message, arguments_string)


def _has_action(arguments):
    return any(arguments.keys()) in __REQUIRE_ACTIONS__

#!/usr/local/bin/python3

import attr

import command
import lookaside_operator

_NEVER_UNITY_TARGETS = ['cpp', 'product', 'web', 'rpm']

_ERROR_FLAGS = [
    'required from here',
    'In member function',
    'error:'
]

_COMPILER_FLAGS = {
    'clang': [
        '-clang'
    ],
    'gcc': []
}


class Builder(lookaside_operator.lookasideOperator):

    def build_targets(self, targets, clang=False, non_unity=False):
        build_targets = list(targets) if non_unity else _get_unity_targets(
            targets)

        compiler_flags = _COMPILER_FLAGS['clang'] if clang else _COMPILER_FLAGS['gcc']

        command_list = ['build'] + compiler_flags + build_targets
        command = ' '.join(command_list)

        self.connection.set_directory_to_cwd()
        result = self.connection.execute_and_print(command)

        if result.failed:
            self.handle_failed_command(result.stdout.full_output)

        return result

    def handle_failed_command(self, output):
        print('Beginning computation...')
        parser = ErrorParser(output)

        print('=' * 50)
        print('= Found error summaries')
        print('=' * 50)

        for string in parser.get_notable_error_strings():
            print(string)
        print('Completed computation...')


class ErrorParser:
    def __init__(self, output_bytes_array):
        self._output = output_bytes_array
        self._in_error = False
        self._current_error = ''
        self._errors = []

    def get_notable_error_strings(self):
        for line in self._output:
            decoded = line.decode('utf-8')
            self._handle_line(decoded)

        if self._in_error:
            self._complete_error()

        return self._errors

    def _handle_line(self, line):
        if self._in_error:
            self._continue_error(line)
        elif self._begins_notable_string(line):
            self._start_error(line)

    def _begins_notable_string(self, line):
        # if any(error_flag in line for error_flag in _ERROR_FLAGS):
        #     print('Found an error...')
        return any(error_flag in line for error_flag in _ERROR_FLAGS)

    def _continue_error(self, line):
        if self._ends_current_error(line):
            self._add_to_current(line)
            self._complete_error()
        elif self._begins_new_error(line):
            self._complete_error()
            self._handle_line(line)
        else:
            self._add_to_current(line)

    def _complete_error(self):
        self._errors.append(self._current_error)
        self._current_error = ''
        self._in_error = False

    def _ends_current_error(self, line):
        return 'make:***' in line

    def _begins_new_error(self, line):
        return 'In file included from' in line

    def _add_to_current(self, line):
        self._current_error += line

    def _start_error(self, line):
        self._current_error = line
        self._in_error = True


def _get_unity_targets(target_list):
    return [target + '_unity' for target in target_list if not _never_unity(target)]


def _never_unity(target):
    return target in _NEVER_UNITY_TARGETS


example_output = [
    b'~/code/dev ~/code/dev\n',
    b'\n',
    b'~/code/dev ~\r\n',
    b'Skipping repository thirdparty/swagger-ui/\r\n',
    b'M\tthirdparty/dpdk\r\n',
    b'M\tthirdparty/libcql\r\n',
    b'M\tthirdparty/zookeeper\r\n',
    b"Already on 'gschrock/I95-15913-extend-test-data-populator'\r\n",
    b'HEAD is now at 9d1ee96 dummy rbuild commit for gschrock/I95-15913-extend-test-data-populator\r\n',
    b'Build farm VM detected\r\n',
    b'Distcc Enabled\r\n',
    b'Setting JOBS (-j) to optimal build farm value: 24\r\n',
    b'Setting DISTCC_HOSTS from file: /Users/gschrock/code/dev/tools/distcc_hosts\r\n',
    b'DISTCC_HOSTS: --randomize 172.29.249.202/12,lzo 172.29.249.206/12,lzo 172.29.249.204/12,lzo\r\n',
    b'Building Locally\r\n',
    b'/Users/gschrock/code/dev/build\r\n',
    b'build: make -j 24  testDataPopulatorTest_unity\r\n',
    b'\x1b[35m\x1b[1mScanning dependencies of target buildtools_setup\x1b[0m\r\n',
    b'[  0%]Built target gen_cpp_stats\r\n',
    b'[  0%] \x1b[34m\x1b[1mCopying Python package buildtools_setup\x1b[0m\r\n',
    b'[  0%] Built target build_pyang_plugins\r\n',
    b'\x1b[34m\x1b[1mStaging libcassandra.so* from /Users/gschrock/code/dev/thirdparty/libcql to /Users/gschrock/code/dev/build/staging/usr/lib64\x1b[0m\r\n',
    b'[  0%] \x1b[34m\x1b[1mSyncing Python wheelhouse\x1b[0m\r\n',
    b'[  0%] \x1b[34m\x1b[1mInitializing Python virtual environments\x1b[0m\r\n',
    b'\x1b[34m\x1b[1mStaging libfolly.so* from /Users/gschrock/code/dev/thirdparty/folly/folly/.libs to /Users/gschrock/code/dev/build/staging/usr/lib64\x1b[0m\r\n',
    b'\x1b[34m\x1b[1mStaging libzookeeper_mt.so* from /Users/gschrock/code/dev/thirdparty/zookeeper/build/c/.libs to /Users/gschrock/code/dev/build/staging/usr/lib64\x1b[0m\r\n',
    b'[  0%] Built target gmock\r\n',
    b'[  0%] Built target folly\r\n',
    b'[  0%] Built target consolidated_t128_model\r\n',
    b'[  0%] Built target cassandra\r\n',
    b'[  0%] Built target consolidated_stats_model\r\n',
    b'\x1b[34m\x1b[1mStaging *.jar from /Users/gschrock/code/dev/thirdparty/zookeeper/build/lib to /Users/gschrock/code/dev/build/staging/usr/lib\x1b[0m\r\n',
    b'[  0%] Built target python_sync_wheelhouse\r\n',
    b'Running virtualenv with interpreter /opt/128technology/bin/python2.7\r\n',
    b'\x1b[34m\x1b[1mStaging *.jar from /Users/gschrock/code/dev/thirdparty/zookeeper/build/lib/external-deps to /Users/gschrock/code/dev/build/staging/usr/lib\x1b[0m\r\n',
    b'[  0%] Built target buildtools_setup\r\n',
    b'\x1b[34m\x1b[1mStaging zookeeper-3.6.0-SNAPSHOT-bin.jar from /Users/gschrock/code/dev/thirdparty/zookeeper/build to /Users/gschrock/code/dev/build/staging/usr/lib\x1b[0m\r\n',
    b'[100%] Built target zookeeper_mt\r\n',
    b'New python executable in /Users/gschrock/code/dev/build/python/.py2env/bin/python2.7\r\n',
    b'Not overwriting existing python script /Users/gschrock/code/dev/build/python/.py2env/bin/python (you must use /Users/gschrock/code/dev/build/python/.py2env/bin/python2.7)\r\n',
    b'Installing setuptools, pip, wheel...done.\r\n',
    b'Running virtualenv with interpreter /opt/128technology/bin/python3.5\r\n',
    b"Using base prefix '/opt/128technology'\r\n",
    b'New python executable in /Users/gschrock/code/dev/build/python/.py3env/bin/python3.5\r\n',
    b'Not overwriting existing python script /Users/gschrock/code/dev/build/python/.py3env/bin/python (you must use /Users/gschrock/code/dev/build/python/.py3env/bin/python3.5)\r\n',
    b'Installing setuptools, pip, wheel...done.\r\n',
    b'[100%] Built target python_virtual_env\r\n',
    b'\x1b[35m\x1b[1mScanning dependencies of target buildtools_build\x1b[0m\r\n',
    b'[100%] \x1b[34m\x1b[1mBuilding wheel for buildtools_build\x1b[0m\r\n',
    b'running bdist_wheel\r\n',
    b'running build\r\n',
    b'running build_py\r\n',
    b'creating build\r\n',
    b'creating build/lib\r\n',
    b'creating build/lib/buildtools\r\n',
    b'copying buildtools/__init__.py -> build/lib/buildtools\r\n',
    b'creating build/lib/buildtools/scripts\r\n',
    b'copying buildtools/scripts/generateConfigMetadata.py -> build/lib/buildtools/scripts\r\n',
    b'copying buildtools/scripts/verifyEditConfigPresentation.py -> build/lib/buildtools/scripts\r\n',
    b'copying buildtools/scripts/generateAnalyticsCppSchemas.py -> build/lib/buildtools/scripts\r\n',
    b'copying buildtools/scripts/generateConfigCppCode.py -> build/lib/buildtools/scripts\r\n',
    b'copying buildtools/scripts/generateStatsJSMetadata.py -> build/lib/buildtools/scripts\r\n',
    b'copying buildtools/scripts/generateEventLogCppSchemas.py -> build/lib/buildtools/scripts\r\n',
    b'copying buildtools/scripts/generateLogEventsSchemas.py -> build/lib/buildtools/scripts\r\n',
    b'copying buildtools/scripts/__init__.py -> build/lib/buildtools/scripts\r\n',
    b'copying buildtools/scripts/generateEditConfigPresentation.py -> build/lib/buildtools/scripts\r\n',
    b'copying buildtools/scripts/generateStatsCppCode.py -> build/lib/buildtools/scripts\r\n',
    b'copying buildtools/scripts/generateAnalyticsCppMaps.py -> build/lib/buildtools/scripts\r\n',
    b'creating build/lib/buildtools/codegen\r\n',
    b'copying buildtools/codegen/database.py -> build/lib/buildtools/codegen\r\n',
    b'copying buildtools/codegen/model.py -> build/lib/buildtools/codegen\r\n',
    b'copying buildtools/codegen/strings.py -> build/lib/buildtools/codegen\r\n',
    b'copying buildtools/codegen/util.py -> build/lib/buildtools/codegen\r\n',
    b'copying buildtools/codegen/cpp.py -> build/lib/buildtools/codegen\r\n',
    b'copying buildtools/codegen/__init__.py -> build/lib/buildtools/codegen\r\n',
    b'installing to build/bdist.linux-x86_64/wheel\r\n',
    b'running install\r\n',
    b'running install_lib\r\n',
    b'creating build/bdist.linux-x86_64\r\n',
    b'creating build/bdist.linux-x86_64/wheel\r\n',
    b'creating build/bdist.linux-x86_64/wheel/buildtools\r\n',
    b'creating build/bdist.linux-x86_64/wheel/buildtools/scripts\r\n',
    b'copying build/lib/buildtools/scripts/generateConfigMetadata.py -> build/bdist.linux-x86_64/wheel/buildtools/scripts\r\n',
    b'copying build/lib/buildtools/scripts/verifyEditConfigPresentation.py -> build/bdist.linux-x86_64/wheel/buildtools/scripts\r\n',
    b'copying build/lib/buildtools/scripts/generateAnalyticsCppSchemas.py -> build/bdist.linux-x86_64/wheel/buildtools/scripts\r\n', b'copying build/lib/buildtools/scripts/generateConfigCppCode.py -> build/bdist.linux-x86_64/wheel/buildtools/scripts\r\n',
    b'copying build/lib/buildtools/scripts/generateStatsJSMetadata.py -> build/bdist.linux-x86_64/wheel/buildtools/scripts\r\n',
    b'copying build/lib/buildtools/scripts/generateEventLogCppSchemas.py -> build/bdist.linux-x86_64/wheel/buildtools/scripts\r\n',
    b'copying build/lib/buildtools/scripts/generateLogEventsSchemas.py -> build/bdist.linux-x86_64/wheel/buildtools/scripts\r\n',
    b'copying build/lib/buildtools/scripts/__init__.py -> build/bdist.linux-x86_64/wheel/buildtools/scripts\r\n',
    b'copying build/lib/buildtools/scripts/generateEditConfigPresentation.py -> build/bdist.linux-x86_64/wheel/buildtools/scripts\r\n',
    b'copying build/lib/buildtools/scripts/generateStatsCppCode.py -> build/bdist.linux-x86_64/wheel/buildtools/scripts\r\n',
    b'copying build/lib/buildtools/scripts/generateAnalyticsCppMaps.py -> build/bdist.linux-x86_64/wheel/buildtools/scripts\r\n',
    b'creating build/bdist.linux-x86_64/wheel/buildtools/codegen\r\n',
    b'copying build/lib/buildtools/codegen/database.py -> build/bdist.linux-x86_64/wheel/buildtools/codegen\r\n',
    b'copying build/lib/buildtools/codegen/model.py -> build/bdist.linux-x86_64/wheel/buildtools/codegen\r\n',
    b'copying build/lib/buildtools/codegen/strings.py -> build/bdist.linux-x86_64/wheel/buildtools/codegen\r\n',
    b'copying build/lib/buildtools/codegen/util.py -> build/bdist.linux-x86_64/wheel/buildtools/codegen\r\n',
    b'copying build/lib/buildtools/codegen/cpp.py -> build/bdist.linux-x86_64/wheel/buildtools/codegen\r\n',
    b'copying build/lib/buildtools/codegen/__init__.py -> build/bdist.linux-x86_64/wheel/buildtools/codegen\r\n',
    b'copying build/lib/buildtools/__init__.py -> build/bdist.linux-x86_64/wheel/buildtools\r\n',
    b'running install_egg_info\r\n',
    b'running egg_info\r\n',
    b'writing top-level names to buildtools.egg-info/top_level.txt\r\n',
    b'writing buildtools.egg-info/PKG-INFO\r\n',
    b'writing dependency_links to buildtools.egg-info/dependency_links.txt\r\n',
    b"reading manifest file 'buildtools.egg-info/SOURCES.txt'\r\n",
    b"writing manifest file 'buildtools.egg-info/SOURCES.txt'\r\n",
    b'Copying buildtools.egg-info to build/bdist.linux-x86_64/wheel/buildtools-0.0.1-py3.5.egg-info\r\n',
    b'running install_scripts\r\n',
    b'creating build/bdist.linux-x86_64/wheel/buildtools-0.0.1.dist-info/WHEEL\r\n',
    b'[100%] Built target buildtools_build\r\n',
    b'\x1b[35m\x1b[1mScanning dependencies of target buildtools\x1b[0m\r\n',
    b'[100%] Built target buildtools\r\n',
    b'[100%] \x1b[34m\x1b[1mInitializing buildtools virtual environment\x1b[0m\r\n',
    b'Running virtualenv with interpreter /opt/128technology/bin/python3.5\r\n',
    b"Using base prefix '/opt/128technology'\r\n",
    b'New python executable in /Users/gschrock/code/dev/build/python/buildtools/.venv/bin/python3.5\r\n',
    b'Also creating executable in /Users/gschrock/code/dev/build/python/buildtools/.venv/bin/python\r\n',
    b'Installing setuptools, pip, wheel...done.\r\n',
    b'Ignoring indexes: https://pypi.python.org/simple\r\n',
    b'Collecting lxml==3.8.0 (from -r /Users/gschrock/code/dev/python/requirements-buildtools-35.txt (line 7))\r\n',
    b'Collecting xpathparser==1.0.0 (from -r /Users/gschrock/code/dev/python/requirements-buildtools-35.txt (line 8))\r\n',
    b'Collecting yinsolidated==1.0.1 (from -r /Users/gschrock/code/dev/python/requirements-buildtools-35.txt (line 9))\r\n',
    b'Installing collected packages: lxml, xpathparser, yinsolidated\r\n',
    b'Successfully installed lxml-3.8.0 xpathparser-1.0.0 yinsolidated-1.0.1\r\n',
    b'Ignoring indexes: https://pypi.python.org/simple\r\n',
    b'Collecting buildtools\r\n',
    b'Installing collected packages: buildtools\r\n',
    b'Successfully installed buildtools-0.0.1\r\n',
    b'[100%] Built target buildtools_virtual_env\r\n',
    b'[100%] Built target generateConfigMetadata\r\n',
    b'[100%] Built target generateEditConfigPresentation\r\n',
    b'[100%] Built target generateLogEventsSchemas\r\n',
    b'[100%] Built target generateConfigCppCode\r\n',
    b'[100%] Built target generateEventLogCppSchemas\r\n',
    b'[100%] Built target generateStatsCppCode\r\n',
    b'[100%] Built target gen_cpp_analytics\r\n',
    b'[100%] Built target generateStatsJSMetadata\r\n',
    b'[100%] Built target generateAnalyticsCppSchemas\r\n',
    b'[100%] Built target generateAnalyticsCppMaps\r\n',
    b'[100%] Built target gen_cpp_analytics_maps\r\n',
    b'[100%] Built target gen_cpp_config\r\n',
    b'[100%] Built target codegen_all\r\n',
    b'[100%] Built target 128T_protobuf_unity\r\n',
    b'[100%] Built target 128T_algo_unity\r\n',
    b'[100%] Built target 128T_gMockMainBase_unity\r\n',
    b'[100%] Built target 128T_util_unity\r\n',
    b'[100%] Built target 128T_crypto_unity\r\n',
    b'[100%] Built target 128T_syslib_unity\r\n',
    b'[100%] Built target 128T_dhcpCommon_unity\r\n',
    b'[100%] Built target 128T_message_unity\r\n',
    b'[100%] Built target 128T_environment_config_unity\r\n',
    b'[100%] Built target 128T_curl_client_unity\r\n',
    b'[100%] Built target 128T_zookeeperSupport_unity\r\n',
    b'[100%] Built target 128T_process_unity\r\n',
    b'[100%] Built target 128T_database_unity\r\n',
    b'[100%] Built target 128T_nonSecureTestingSupport_unity\r\n',
    b'[100%] Built target 128T_vtysh_unity\r\n',
    b'[100%] Built target 128T_persistentData_unity\r\n',
    b'[100%] Built target 128T_net_unity\r\n',
    b'[100%] Built target 128T_stats_unity\r\n',
    b'[100%] Built target 128T_config_base_unity\r\n',
    b'[100%] Built target 128T_dhcpClient_unity\r\n',
    b'[100%] Built target 128T_state_unity\r\n',
    b'[100%] Built target 128T_config_unity\r\n',
    b'[100%] Built target 128T_gMockMain_unity\r\n',
    b'[100%] Built target 128T_app_unity\r\n',
    b'[100%] Built target 128T_analytics_unity\r\n',
    b'[100%] Built target 128T_testDataPopulator_unity\r\n',
    b'[100%] \x1b[32mBuilding CXX object tools/metrics/test_data_populator/test/CMakeFiles/testDataPopulatorTest_unity.dir/cotire/testDataPopulatorTest_CXX_unity.cxx.o\x1b[0m\r\n',
    b'distcc[3751] ERROR: compile /Users/gschrock/code/dev/build/tools/metrics/test_data_populator/test/cotire/testDataPopulatorTest_CXX_unity.cxx on 172.29.249.204/12,lzo failed\r\n',
    b'In file included from /Users/gschrock/code/dev/build/tools/metrics/test_data_populator/test/cotire/testDataPopulatorTest_CXX_unity.cxx:4:0:\r\n',
    b"/Users/gschrock/code/dev/tools/metrics/test_data_populator/test/AnalyticsPopulatorTest.cpp: In member function 'virtual void AnalyticsPopulatorTest::SetUp()':\r\n",
    b"/Users/gschrock/code/dev/tools/metrics/test_data_populator/test/AnalyticsPopulatorTest.cpp:29:49: error: 'database::CassandraSchemaPolicy' has not been declared\r\n",
    b'In file included from /Users/gschrock/code/dev/build/tools/metrics/test_data_populator/test/cotire/testDataPopulatorTest_CXX_unity.cxx:5:0:\r\n',
    b"/Users/gschrock/code/dev/tools/metrics/test_data_populator/test/CsvDataTest.cpp: In member function 'virtual void CsvData_SingleRow_Test::TestBody()':\r\n",
    b"/Users/gschrock/code/dev/tools/metrics/test_data_populator/test/CsvDataTest.cpp:72:71: error: no matching function for call to 'database::CsvRow::CsvRow(<brace-enclosed initializer list>)'\r\n",
    b'In file included from /Users/gschrock/code/dev/tools/metrics/test_data_populator/test/../DataPopulator.hpp:8:0,\r\n',
    b' from /Users/gschrock/code/dev/tools/metrics/test_data_populator/test/../AnalyticsPopulator.hpp:8,\r\n',
    b'                 from /Users/gschrock/code/dev/tools/metrics/test_data_populator/test/AnalyticsPopulatorTest.cpp:6,\r\n',
    b'                 from /Users/gschrock/code/dev/build/tools/metrics/test_data_populator/test/cotire/testDataPopulatorTest_CXX_unity.cxx:4:\r\n',
    b'/Users/gschrock/code/dev/tools/metrics/test_data_populator/test/../CsvData.hpp:61:5: note: candidate: database::CsvRow::CsvRow(database::CsvRow&&)\r\n',
    b'/Users/gschrock/code/dev/tools/metrics/test_data_populator/test/../CsvData.hpp:61:5: note:   candidate expects 1 argument, 3 provided\r\n',
    b'/Users/gschrock/code/dev/tools/metrics/test_data_populator/test/../CsvData.hpp:60:5: note: candidate: database::CsvRow::CsvRow(const database::CsvRow&)\r\n',
    b'/Users/gschrock/code/dev/tools/metrics/test_data_populator/test/../CsvData.hpp:60:5: note:   candidate expects 1 argument, 3 provided\r\n',
    b'/Users/gschrock/code/dev/tools/metrics/test_data_populator/test/../CsvData.hpp:56:5: note: candidate: database::CsvRow::CsvRow(const MapType&)\r\n',
    b'/Users/gschrock/code/dev/tools/metrics/test_data_populator/test/../CsvData.hpp:56:5: note:   candidate expects 1 argument, 3 provided\r\n',
    b'/Users/gschrock/code/dev/tools/metrics/test_data_populator/test/../CsvData.hpp:55:5: note: candidate: database::CsvRow::CsvRow()\r\n',
    b'/Users/gschrock/code/dev/tools/metrics/test_data_populator/test/../CsvData.hpp:55:5: note:   candidate expects 0 arguments, 3 provided\r\n',
    b'In file included from /Users/gschrock/code/dev/build/tools/metrics/test_data_populator/test/cotire/testDataPopulatorTest_CXX_unity.cxx:5:0:\r\n',
    b"/Users/gschrock/code/dev/tools/metrics/test_data_populator/test/CsvDataTest.cpp: In member function 'virtual void CsvData_MultipleRows_Test::TestBody()':\r\n",
    b"/Users/gschrock/code/dev/tools/metrics/test_data_populator/test/CsvDataTest.cpp:88:76: error: no matching function for call to 'database::CsvRow::CsvRow(<brace-enclosed initializer list>)'\r\n",
    b'In file included from /Users/gschrock/code/dev/tools/metrics/test_data_populator/test/../DataPopulator.hpp:8:0,\r\n',
    b'                 from /Users/gschrock/code/dev/tools/metrics/test_data_populator/test/../AnalyticsPopulator.hpp:8,\r\n',
    b'    from /Users/gschrock/code/dev/tools/metrics/test_data_populator/test/AnalyticsPopulatorTest.cpp:6,\r\n',
    b'                 from /Users/gschrock/code/dev/build/tools/metrics/test_data_populator/test/cotire/testDataPopulatorTest_CXX_unity.cxx:4:\r\n',
    b'/Users/gschrock/code/dev/tools/metrics/test_data_populator/test/../CsvData.hpp:61:5: note: candidate: database::CsvRow::CsvRow(database::CsvRow&&)\r\n',
    b'/Users/gschrock/code/dev/tools/metrics/test_data_populator/test/../CsvData.hpp:61:5: note:   candidate expects 1 argument, 3 provided\r\n',
    b'/Users/gschrock/code/dev/tools/metrics/test_data_populator/test/../CsvData.hpp:60:5: note: candidate: database::CsvRow::CsvRow(const database::CsvRow&)\r\n',
    b'/Users/gschrock/code/dev/tools/metrics/test_data_populator/test/../CsvData.hpp:60:5: note:   candidate expects 1 argument, 3 provided\r\n',
    b'/Users/gschrock/code/dev/tools/metrics/test_data_populator/test/../CsvData.hpp:56:5: note: candidate: database::CsvRow::CsvRow(const MapType&)\r\n',
    b'/Users/gschrock/code/dev/tools/metrics/test_data_populator/test/../CsvData.hpp:56:5: note:   candidate expects 1 argument, 3 provided\r\n',
    b'/Users/gschrock/code/dev/tools/metrics/test_data_populator/test/../CsvData.hpp:55:5: note: candidate: database::CsvRow::CsvRow()\r\n',
    b'/Users/gschrock/code/dev/tools/metrics/test_data_populator/test/../CsvData.hpp:55:5: note:   candidate expects 0 arguments, 3 provided\r\n',
    b'In file included from /Users/gschrock/code/dev/build/tools/metrics/test_data_populator/test/cotire/testDataPopulatorTest_CXX_unity.cxx:5:0:\r\n',
    b"/Users/gschrock/code/dev/tools/metrics/test_data_populator/test/CsvDataTest.cpp:91:77: error: no matching function for call to 'database::CsvRow::CsvRow(<brace-enclosed initializer list>)'\r\n",
    b'In file included from /Users/gschrock/code/dev/tools/metrics/test_data_populator/test/../DataPopulator.hpp:8:0,\r\n',
    b'                 from /Users/gschrock/code/dev/tools/metrics/test_data_populator/test/../AnalyticsPopulator.hpp:8,\r\n',
    b'           from /Users/gschrock/code/dev/tools/metrics/test_data_populator/test/AnalyticsPopulatorTest.cpp:6,\r\n',
    b'                 from /Users/gschrock/code/dev/build/tools/metrics/test_data_populator/test/cotire/testDataPopulatorTest_CXX_unity.cxx:4:\r\n',
    b'/Users/gschrock/code/dev/tools/metrics/test_data_populator/test/../CsvData.hpp:61:5: note: candidate: database::CsvRow::CsvRow(database::CsvRow&&)\r\n',
    b'/Users/gschrock/code/dev/tools/metrics/test_data_populator/test/../CsvData.hpp:61:5: note:   candidate expects 1 argument, 3 provided\r\n',
    b'/Users/gschrock/code/dev/tools/metrics/test_data_populator/test/../CsvData.hpp:60:5: note: candidate: database::CsvRow::CsvRow(const database::CsvRow&)\r\n',
    b'/Users/gschrock/code/dev/tools/metrics/test_data_populator/test/../CsvData.hpp:60:5: note:   candidate expects 1 argument, 3 provided\r\n',
    b'/Users/gschrock/code/dev/tools/metrics/test_data_populator/test/../CsvData.hpp:56:5: note: candidate: database::CsvRow::CsvRow(const MapType&)\r\n',
    b'/Users/gschrock/code/dev/tools/metrics/test_data_populator/test/../CsvData.hpp:56:5: note:   candidate expects 1 argument, 3 provided\r\n',
    b'/Users/gschrock/code/dev/tools/metrics/test_data_populator/test/../CsvData.hpp:55:5: note: candidate: database::CsvRow::CsvRow()\r\n',
    b'/Users/gschrock/code/dev/tools/metrics/test_data_populator/test/../CsvData.hpp:55:5: note:   candidate expects 0 arguments, 3 provided\r\n',
    b'In file included from /Users/gschrock/code/dev/build/tools/metrics/test_data_populator/test/cotire/testDataPopulatorTest_CXX_unity.cxx:5:0:\r\n',
    b"/Users/gschrock/code/dev/tools/metrics/test_data_populator/test/CsvDataTest.cpp: In member function 'virtual void CsvData_Sort_Test::TestBody()':\r\n",
    b"/Users/gschrock/code/dev/tools/metrics/test_data_populator/test/CsvDataTest.cpp:130:5: error: no matching function for call to 'folly::fbvector<database::CsvRow>::fbvector(<brace-enclosed initializer list>)'\r\n",
    b'In file included from /Users/gschrock/code/dev/thirdparty/folly/folly/String.h:39:0,\r\n',
    b'                 from /Users/gschrock/code/dev/src/lib/util/Log.hpp:13,\r\n',
    b'                 from /Users/gschrock/code/dev/src/lib/util/Exception-inl.hpp:8,\r\n',
    b'                 from /Users/gschrock/code/dev/src/lib/util/Exception.hpp:17,\r\n',
    b'       from /Users/gschrock/code/dev/tools/metrics/test_data_populator/test/../CsvData.hpp:8,\r\n',
    b'                 from /Users/gschrock/code/dev/tools/metrics/test_data_populator/test/../DataPopulator.hpp:8,\r\n',
    b'                 from /Users/gschrock/code/dev/tools/metrics/test_data_populator/test/../AnalyticsPopulator.hpp:8,\r\n',
    b'                 from /Users/gschrock/code/dev/tools/metrics/test_data_populator/test/AnalyticsPopulatorTest.cpp:6,\r\n',
    b'                 from /Users/gschrock/code/dev/build/tools/metrics/test_data_populator/test/cotire/testDataPopulatorTest_CXX_unity.cxx:4:\r\n',
    b'/Users/gschrock/code/dev/thirdparty/folly/folly/FBVector.h:801:3: note: candidate: template<class InputIterator> folly::fbvector<T, Allocator>::fbvector(InputIterator, InputIterator, const Allocator&, std::input_iterator_tag)\r\n',
    b'/Users/gschrock/code/dev/thirdparty/folly/folly/FBVector.h:801:3: note:   template argument deduction/substitution failed:\r\n',
    b'In file included from /Users/gschrock/code/dev/build/tools/metrics/test_data_populator/test/cotire/testDataPopulatorTest_CXX_unity.cxx:5:0:\r\n',
    b'/Users/gschrock/code/dev/tools/metrics/test_data_populator/test/CsvDataTest.cpp:130:5: note:   candidate expects 4 arguments, 3 provided\r\n',
    b'In file included from /Users/gschrock/code/dev/thirdparty/folly/folly/String.h:39:0,\r\n',
    b'                 from /Users/gschrock/code/dev/src/lib/util/Log.hpp:13,\r\n',
    b'                from /Users/gschrock/code/dev/src/lib/util/Exception-inl.hpp:8,\r\n',
    b'                 from /Users/gschrock/code/dev/src/lib/util/Exception.hpp:17,\r\n',
    b'                 from /Users/gschrock/code/dev/tools/metrics/test_data_populator/test/../CsvData.hpp:8,\r\n',
    b'              from /Users/gschrock/code/dev/tools/metrics/test_data_populator/test/../DataPopulator.hpp:8,\r\n',
    b'                 from /Users/gschrock/code/dev/tools/metrics/test_data_populator/test/../AnalyticsPopulator.hpp:8,\r\n',
    b'                 from /Users/gschrock/code/dev/tools/metrics/test_data_populator/test/AnalyticsPopulatorTest.cpp:6,\r\n',
    b'                 from /Users/gschrock/code/dev/build/tools/metrics/test_data_populator/test/cotire/testDataPopulatorTest_CXX_unity.cxx:4:\r\n',
    b'/Users/gschrock/code/dev/thirdparty/folly/folly/FBVector.h:795:3: note: candidate: template<class ForwardIterator> folly::fbvector<T, Allocator>::fbvector(ForwardIterator, ForwardIterator, const Allocator&, std::forward_iterator_tag)\r\n',
    b'/Users/gschrock/code/dev/thirdparty/folly/folly/FBVector.h:795:3: note:   template argument deduction/substitution failed:\r\n',
    b'In file included from /Users/gschrock/code/dev/build/tools/metrics/test_data_populator/test/cotire/testDataPopulatorTest_CXX_unity.cxx:5:0:\r\n',
    b'/Users/gschrock/code/dev/tools/metrics/test_data_populator/test/CsvDataTest.cpp:130:5: note:   candidate expects 4 arguments, 3 provided\r\n',
    b'In file included from /Users/gschrock/code/dev/thirdparty/folly/folly/String.h:39:0,\r\n',
    b'                 from /Users/gschrock/code/dev/src/lib/util/Log.hpp:13,\r\n',
    b'                 from /Users/gschrock/code/dev/src/lib/util/Exception-inl.hpp:8,\r\n',
    b'        from /Users/gschrock/code/dev/src/lib/util/Exception.hpp:17,\r\n',
    b'                 from /Users/gschrock/code/dev/tools/metrics/test_data_populator/test/../CsvData.hpp:8,\r\n',
    b'                 from /Users/gschrock/code/dev/tools/metrics/test_data_populator/test/../DataPopulator.hpp:8,\r\n',
    b'                 from /Users/gschrock/code/dev/tools/metrics/test_data_populator/test/../AnalyticsPopulator.hpp:8,\r\n',
    b'                 from /Users/gschrock/code/dev/tools/metrics/test_data_populator/test/AnalyticsPopulatorTest.cpp:6,\r\n',
    b'                 from /Users/gschrock/code/dev/build/tools/metrics/test_data_populator/test/cotire/testDataPopulatorTest_CXX_unity.cxx:4:\r\n',
    b'/Users/gschrock/code/dev/thirdparty/folly/folly/FBVector.h:723:3: note: candidate: folly::fbvector<T, Allocator>::fbvector(std::initializer_list<_Tp>, const Allocator&) [with T = database::CsvRow; Allocator = std::allocator<database::CsvRow>]\r\n',
    b'/Users/gschrock/code/dev/thirdparty/folly/folly/FBVector.h:723:3: note:   candidate expects 2 arguments, 3 provided\r\n',
    b'/Users/gschrock/code/dev/thirdparty/folly/folly/FBVector.h:714:19: note: candidate: folly::fbvector<T, Allocator>::fbvector(folly::fbvector<T, Allocator>&&, const Allocator&) [with T = database::CsvRow; Allocator = std::allocator<database::CsvRow>]\r\n',
    b'/Users/gschrock/code/dev/thirdparty/folly/folly/FBVector.h:714:19: note:   candidate expects 2 arguments, 3 provided\r\n',
    b'/Users/gschrock/code/dev/thirdparty/folly/folly/FBVector.h:711:3: note: candidate: folly::fbvector<T, Allocator>::fbvector(const folly::fbvector<T, Allocator>&, const Allocator&) [with T = database::CsvRow; Allocator = std::allocator<database::CsvRow>]\r\n',
    b'/Users/gschrock/code/dev/thirdparty/folly/folly/FBVector.h:711:3: note:   candidate expects 2 arguments, 3 provided\r\n',
    b'/Users/gschrock/code/dev/thirdparty/folly/folly/FBVector.h:709:3: note: candidate: folly::fbvector<T, Allocator>::fbvector(folly::fbvector<T, Allocator>&&) [with T = database::CsvRow; Allocator = std::allocator<database::CsvRow>]\r\n',
    b'/Users/gschrock/code/dev/thirdparty/folly/folly/FBVector.h:709:3: note:   candidate expects 1 argument, 3 provided\r\n',
    b'/Users/gschrock/code/dev/thirdparty/folly/folly/FBVector.h:705:3: note: candidate: folly::fbvector<T, Allocator>::fbvector(const folly::fbvector<T, Allocator>&) [with T = database::CsvRow; Allocator = std::allocator<database::CsvRow>]\r\n',
    b'/Users/gschrock/code/dev/thirdparty/folly/folly/FBVector.h:705:3: note:   candidate expects 1 argument, 3 provided\r\n',
    b'/Users/gschrock/code/dev/thirdparty/folly/folly/FBVector.h:702:3: note: candidate: template<class It, class Category> folly::fbvector<T, Allocator>::fbvector(It, It, const Allocator&)\r\n',
    b'/Users/gschrock/code/dev/thirdparty/folly/folly/FBVector.h:702:3: note:   template argument deduction/substitution failed:\r\n', b'In file included from /Users/gschrock/code/dev/build/tools/metrics/test_data_populator/test/cotire/testDataPopulatorTest_CXX_unity.cxx:5:0:\r\n',
    b"/Users/gschrock/code/dev/tools/metrics/test_data_populator/test/CsvDataTest.cpp:130:5: note:   couldn't deduce template parameter 'It'\r\n",
    b'In file included from /Users/gschrock/code/dev/thirdparty/folly/folly/String.h:39:0,\r\n',
    b'                 from /Users/gschrock/code/dev/src/lib/util/Log.hpp:13,\r\n',
    b'                 from /Users/gschrock/code/dev/src/lib/util/Exception-inl.hpp:8,\r\n',
    b'                 from /Users/gschrock/code/dev/src/lib/util/Exception.hpp:17,\r\n',
    b'                 from /Users/gschrock/code/dev/tools/metrics/test_data_populator/test/../CsvData.hpp:8,\r\n',
    b'                 from /Users/gschrock/code/dev/tools/metrics/test_data_populator/test/../DataPopulator.hpp:8,\r\n',
    b'                 from /Users/gschrock/code/dev/tools/metrics/test_data_populator/test/../AnalyticsPopulator.hpp:8,\r\n',
    b'                 from /Users/gschrock/code/dev/tools/metrics/test_data_populator/test/AnalyticsPopulatorTest.cpp:6,\r\n',
    b'                 from /Users/gschrock/code/dev/build/tools/metrics/test_data_populator/test/cotire/testDataPopulatorTest_CXX_unity.cxx:4:\r\n',
    b'/Users/gschrock/code/dev/thirdparty/folly/folly/FBVector.h:696:3: note: candidate: folly::fbvector<T, Allocator>::fbvector(folly::fbvector<T, Allocator>::size_type, folly::fbvector<T, Allocator>::VT, const Allocator&) [with T = database::CsvRow; Allocator = std::allocator<database::CsvRow>; folly::fbvector<T, Allocator>::size_type = long unsigned int; folly::fbvector<T, Allocator>::VT = const database::CsvRow&]\r\n',
    b"/Users/gschrock/code/dev/thirdparty/folly/folly/FBVector.h:696:3: note:   no known conversion for argument 1 from '<brace-enclosed initializer list>' to 'folly::fbvector<database::CsvRow>::size_type {aka long unsigned int}'\r\n",
    b'/Users/gschrock/code/dev/thirdparty/folly/folly/FBVector.h:692:12: note: candidate: folly::fbvector<T, Allocator>::fbvector(folly::fbvector<T, Allocator>::size_type, const Allocator&) [with T = database::CsvRow; Allocator = std::allocator<database::CsvRow>; folly::fbvector<T, Allocator>::size_type = long unsigned int]\r\n',
    b'/Users/gschrock/code/dev/thirdparty/folly/folly/FBVector.h:692:12: note:   candidate expects 2 arguments, 3 provided\r\n',
    b'/Users/gschrock/code/dev/thirdparty/folly/folly/FBVector.h:690:12: note: candidate: folly::fbvector<T, Allocator>::fbvector(const Allocator&) [with T = database::CsvRow; Allocator = std::allocator<database::CsvRow>]\r\n',
    b'/Users/gschrock/code/dev/thirdparty/folly/folly/FBVector.h:690:12: note:   candidate expects 1 argument, 3 provided\r\n',
    b'/Users/gschrock/code/dev/thirdparty/folly/folly/FBVector.h:688:3: note: candidate: folly::fbvector<T, Allocator>::fbvector() [with T = database::CsvRow; Allocator = std::allocator<database::CsvRow>]\r\n',
    b'/Users/gschrock/code/dev/thirdparty/folly/folly/FBVector.h:688:3: note:   candidate expects 0 arguments, 3 provided\r\n',
    b'In file included from /Users/gschrock/code/dev/build/tools/metrics/test_data_populator/test/cotire/testDataPopulatorTest_CXX_unity.cxx:5:0:\r\n',
    b"/Users/gschrock/code/dev/tools/metrics/test_data_populator/test/CsvDataTest.cpp:138:5: error: no matching function for call to 'folly::fbvector<database::CsvRow>::fbvector(<brace-enclosed initializer list>)'\r\n",
    b'In file included from /Users/gschrock/code/dev/thirdparty/folly/folly/String.h:39:0,\r\n',
    b'                 from /Users/gschrock/code/dev/src/lib/util/Log.hpp:13,\r\n',
    b'                 from /Users/gschrock/code/dev/src/lib/util/Exception-inl.hpp:8,\r\n',
    b'                 from /Users/gschrock/code/dev/src/lib/util/Exception.hpp:17,\r\n',
    b'                 from /Users/gschrock/code/dev/tools/metrics/test_data_populator/test/../CsvData.hpp:8,\r\n',
    b'                 from /Users/gschrock/code/dev/tools/metrics/test_data_populator/test/../DataPopulator.hpp:8,\r\n',
    b'                 from /Users/gschrock/code/dev/tools/metrics/test_data_populator/test/../AnalyticsPopulator.hpp:8,\r\n',
    b'                 from /Users/gschrock/code/dev/tools/metrics/test_data_populator/test/AnalyticsPopulatorTest.cpp:6,\r\n',
    b'                 from /Users/gschrock/code/dev/build/tools/metrics/test_data_populator/test/cotire/testDataPopulatorTest_CXX_unity.cxx:4:\r\n',
    b'/Users/gschrock/code/dev/thirdparty/folly/folly/FBVector.h:801:3: note: candidate: template<class InputIterator> folly::fbvector<T, Allocator>::fbvector(InputIterator, InputIterator, const Allocator&, std::input_iterator_tag)\r\n',
    b'/Users/gschrock/code/dev/thirdparty/folly/folly/FBVector.h:801:3: note:   template argument deduction/substitution failed:\r\n',
    b'In file included from /Users/gschrock/code/dev/build/tools/metrics/test_data_populator/test/cotire/testDataPopulatorTest_CXX_unity.cxx:5:0:\r\n',
    b'/Users/gschrock/code/dev/tools/metrics/test_data_populator/test/CsvDataTest.cpp:138:5: note:   candidate expects 4 arguments, 3 provided\r\n',
    b'In file included from /Users/gschrock/code/dev/thirdparty/folly/folly/String.h:39:0,\r\n',
    b'                 from /Users/gschrock/code/dev/src/lib/util/Log.hpp:13,\r\n',
    b'                 from /Users/gschrock/code/dev/src/lib/util/Exception-inl.hpp:8,\r\n',
    b'                 from /Users/gschrock/code/dev/src/lib/util/Exception.hpp:17,\r\n',
    b'                 from /Users/gschrock/code/dev/tools/metrics/test_data_populator/test/../CsvData.hpp:8,\r\n',
    b'                 from /Users/gschrock/code/dev/tools/metrics/test_data_populator/test/../DataPopulator.hpp:8,\r\n',
    b'                 from /Users/gschrock/code/dev/tools/metrics/test_data_populator/test/../AnalyticsPopulator.hpp:8,\r\n',
    b'                 from /Users/gschrock/code/dev/tools/metrics/test_data_populator/test/AnalyticsPopulatorTest.cpp:6,\r\n',
    b'                 from /Users/gschrock/code/dev/build/tools/metrics/test_data_populator/test/cotire/testDataPopulatorTest_CXX_unity.cxx:4:\r\n',
    b'/Users/gschrock/code/dev/thirdparty/folly/folly/FBVector.h:795:3: note: candidate: template<class ForwardIterator> folly::fbvector<T, Allocator>::fbvector(ForwardIterator, ForwardIterator, const Allocator&, std::forward_iterator_tag)\r\n',
    b'/Users/gschrock/code/dev/thirdparty/folly/folly/FBVector.h:795:3: note:   template argument deduction/substitution failed:\r\n',
    b'In file included from /Users/gschrock/code/dev/build/tools/metrics/test_data_populator/test/cotire/testDataPopulatorTest_CXX_unity.cxx:5:0:\r\n',
    b'/Users/gschrock/code/dev/tools/metrics/test_data_populator/test/CsvDataTest.cpp:138:5: note:   candidate expects 4 arguments, 3 provided\r\n',
    b'In file included from /Users/gschrock/code/dev/thirdparty/folly/folly/String.h:39:0,\r\n',
    b'                 from /Users/gschrock/code/dev/src/lib/util/Log.hpp:13,\r\n',
    b'                 from /Users/gschrock/code/dev/src/lib/util/Exception-inl.hpp:8,\r\n',
    b'                 from /Users/gschrock/code/dev/src/lib/util/Exception.hpp:17,\r\n',
    b'                 from /Users/gschrock/code/dev/tools/metrics/test_data_populator/test/../CsvData.hpp:8,\r\n',
    b'                 from /Users/gschrock/code/dev/tools/metrics/test_data_populator/test/../DataPopulator.hpp:8,\r\n',
    b'                 from /Users/gschrock/code/dev/tools/metrics/test_data_populator/test/../AnalyticsPopulator.hpp:8,\r\n',
    b'                 from /Users/gschrock/code/dev/tools/metrics/test_data_populator/test/AnalyticsPopulatorTest.cpp:6,\r\n',
    b'         from /Users/gschrock/code/dev/build/tools/metrics/test_data_populator/test/cotire/testDataPopulatorTest_CXX_unity.cxx:4:\r\n',
    b'/Users/gschrock/code/dev/thirdparty/folly/folly/FBVector.h:723:3: note: candidate: folly::fbvector<T, Allocator>::fbvector(std::initializer_list<_Tp>,const Allocator&) [with T = database::CsvRow; Allocator = std::allocator<database::CsvRow>]\r\n',
    b'/Users/gschrock/code/dev/thirdparty/folly/folly/FBVector.h:723:3: note:   candidate expects 2 arguments, 3 provided\r\n',
    b'/Users/gschrock/code/dev/thirdparty/folly/folly/FBVector.h:714:19: note: candidate: folly::fbvector<T, Allocator>::fbvector(folly::fbvector<T, Allocator>&&, const Allocator&) [with T = database::CsvRow; Allocator = std::allocator<database::CsvRow>]\r\n',
    b'/Users/gschrock/code/dev/thirdparty/folly/folly/FBVector.h:714:19: note:   candidate expects 2 arguments, 3 provided\r\n',
    b'/Users/gschrock/code/dev/thirdparty/folly/folly/FBVector.h:711:3: note: candidate: folly::fbvector<T, Allocator>::fbvector(const folly::fbvector<T, Allocator>&, const Allocator&) [with T = database::CsvRow; Allocator = std::allocator<database::CsvRow>]\r\n',
    b'/Users/gschrock/code/dev/thirdparty/folly/folly/FBVector.h:711:3: note:   candidate expects 2 arguments, 3 provided\r\n',
    b'/Users/gschrock/code/dev/thirdparty/folly/folly/FBVector.h:709:3: note: candidate: folly::fbvector<T, Allocator>::fbvector(folly::fbvector<T, Allocator>&&) [with T = database::CsvRow; Allocator = std::allocator<database::CsvRow>]\r\n',
    b'/Users/gschrock/code/dev/thirdparty/folly/folly/FBVector.h:709:3: note:   candidate expects 1 argument, 3 provided\r\n',
    b'/Users/gschrock/code/dev/thirdparty/folly/folly/FBVector.h:705:3: note: candidate: folly::fbvector<T, Allocator>::fbvector(const folly::fbvector<T, Allocator>&) [with T = database::CsvRow; Allocator = std::allocator<database::CsvRow>]\r\n',
    b'/Users/gschrock/code/dev/thirdparty/folly/folly/FBVector.h:705:3: note:   candidate expects 1 argument, 3 provided\r\n',
    b'/Users/gschrock/code/dev/thirdparty/folly/folly/FBVector.h:702:3: note: candidate: template<class It, class Category> folly::fbvector<T, Allocator>::fbvector(It, It, const Allocator&)\r\n',
    b'/Users/gschrock/code/dev/thirdparty/folly/folly/FBVector.h:702:3: note:   template argument deduction/substitution failed:\r\n',
    b'In file included from /Users/gschrock/code/dev/build/tools/metrics/test_data_populator/test/cotire/testDataPopulatorTest_CXX_unity.cxx:5:0:\r\n',
    b"/Users/gschrock/code/dev/tools/metrics/test_data_populator/test/CsvDataTest.cpp:138:5: note:   couldn't deduce template parameter 'It'\r\n",
    b'In file included from /Users/gschrock/code/dev/thirdparty/folly/folly/String.h:39:0,\r\n',
    b'                 from /Users/gschrock/code/dev/src/lib/util/Log.hpp:13,\r\n',
    b'                 from /Users/gschrock/code/dev/src/lib/util/Exception-inl.hpp:8,\r\n',
    b'          from /Users/gschrock/code/dev/src/lib/util/Exception.hpp:17,\r\n',
    b'                 from /Users/gschrock/code/dev/tools/metrics/test_data_populator/test/../CsvData.hpp:8,\r\n',
    b'                 from /Users/gschrock/code/dev/tools/metrics/test_data_populator/test/../DataPopulator.hpp:8,\r\n',
    b'                 from /Users/gschrock/code/dev/tools/metrics/test_data_populator/test/../AnalyticsPopulator.hpp:8,\r\n',
    b'                 from /Users/gschrock/code/dev/tools/metrics/test_data_populator/test/AnalyticsPopulatorTest.cpp:6,\r\n',
    b'                 from/Users/gschrock/code/dev/build/tools/metrics/test_data_populator/test/cotire/testDataPopulatorTest_CXX_unity.cxx:4:\r\n',
    b'/Users/gschrock/code/dev/thirdparty/folly/folly/FBVector.h:696:3: note: candidate: folly::fbvector<T, Allocator>::fbvector(folly::fbvector<T, Allocator>::size_type,folly::fbvector<T, Allocator>::VT, const Allocator&) [with T = database::CsvRow; Allocator = std::allocator<database::CsvRow>; folly::fbvector<T, Allocator>::size_type = long unsigned int; folly::fbvector<T, Allocator>::VT = const database::CsvRow&]\r\n',
    b"/Users/gschrock/code/dev/thirdparty/folly/folly/FBVector.h:696:3: note:   no known conversion for argument 1 from '<brace-enclosed initializer list>' to 'folly::fbvector<database::CsvRow>::size_type {aka long unsigned int}'\r\n",
    b'/Users/gschrock/code/dev/thirdparty/folly/folly/FBVector.h:692:12: note: candidate: folly::fbvector<T, Allocator>::fbvector(folly::fbvector<T, Allocator>::size_type, const Allocator&) [with T = database::CsvRow; Allocator = std::allocator<database::CsvRow>; folly::fbvector<T, Allocator>::size_type = long unsigned int]\r\n',
    b'/Users/gschrock/code/dev/thirdparty/folly/folly/FBVector.h:692:12: note:   candidate expects 2 arguments, 3 provided\r\n',
    b'/Users/gschrock/code/dev/thirdparty/folly/folly/FBVector.h:690:12: note: candidate: folly::fbvector<T, Allocator>::fbvector(const Allocator&) [with T = database::CsvRow; Allocator = std::allocator<database::CsvRow>]\r\n',
    b'/Users/gschrock/code/dev/thirdparty/folly/folly/FBVector.h:690:12: note:   candidate expects 1 argument, 3 provided\r\n',
    b'/Users/gschrock/code/dev/thirdparty/folly/folly/FBVector.h:688:3: note: candidate: folly::fbvector<T, Allocator>::fbvector() [with T = database::CsvRow; Allocator = std::allocator<database::CsvRow>]\r\n',
    b'/Users/gschrock/code/dev/thirdparty/folly/folly/FBVector.h:688:3: note:   candidate expects 0 arguments, 3 provided\r\n',
    b'In file i']


if __name__ == '__main__':
    parser = ErrorParser(example_output)
    for error in parser.get_notable_error_strings():
        print(error)

import attr


class CompileError(Exception):
    def __init__(self, command, result):
        super().__init__(f'Compilation failed: {command}')
        self.command = command
        self.result = result


@attr.s
class CMakeFlags:

    @property
    def list(self):
        return [] if self.use_cotire else ['-nocotire']


@attr.s
class Flags:
    use_clang = attr.ib(default=False)
    use_unity = attr.ib(default=False)
    use_cotire = attr.ib(default=False)
    jobs = attr.ib(default=None)

    @property
    def list(self):
        flags = []

        if self.use_clang:
            flags.append('-clang')

        if not self.use_cotire:
            flags.append('-nocotire')

        if self.jobs:
            flags.extend(["-j", str(self.jobs)])

        return flags


@attr.s
class Compiler:
    connection = attr.ib()
    flags = attr.ib(default=attr.Factory(Flags))

    def compile(self, targets):
        command_list = ['build'] + self.flags.list + self.get_targets(targets)
        command_str = ' '.join(command_list)

        print('Executing:', command_str)
        return self.connection.execute_and_print(command_str)

    def get_targets(self, targets):
        if self.flags.use_unity:
            return targets.as_unity()
        else:
            return targets.as_non_unity()

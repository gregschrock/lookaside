#!/usr/local/bin/python3

import build
import click
import notify
import file_relations
import filters
import git
import ssh
import targets
import test


@click.command("file")
@click.argument('file-path', type=click.Path(exists=True))
def test_file(file_path):
    """Runs remotely the test associated with the specified files"""

    unit_test = file_relations.get_unit_test_path(file_path)
    if unit_test is None:
        print('No test_file tests found for ' + file_path)
        exit(0)

    executables = [targets.find_target_for(unit_test)]
    target_filters = filters.get_filters_for_tests_in(unit_test)

    if executables[0] is not None:
        with ssh.connect_via_ssh(host='vm1') as connection:
            git.push_to_remote(connection)
            builder = build.Builder(connection)
            build_result = builder.build_targets(executables)
            if not build_result.failed:
                test.run_targets(connection, executables, target_filters)
        # except commands.CommandFailed as e:
        #     print(e)
    else:
        print("Could not find targets to run")
        notify.notify("Could not find targets to run")


if __name__ == '__main__':
    test_file()

import attr
import pathlib

import git

from lookaside.clients import local
from lookaside.clients import ssh


DUMMY_COMMIT_SIGNAL = 'dummy rbuild commit for'


@attr.s
class Repo:
    _repo = attr.ib()

    @classmethod
    def create(cls, path=pathlib.Path.cwd()):
        return cls(git.Repo(str(path), search_parent_directories=True))

    @property
    def root(self):
        return pathlib.Path(self._repo.working_tree_dir).resolve()

    @property
    def is_dirty(self):
        return self._repo.is_dirty(untracked_files=True)

    @property
    def last_commit(self):
        return self._repo.head.reference.commit

    @property
    def has_dummy_commit(self):
        return self.last_commit.summary.startswith(DUMMY_COMMIT_SIGNAL)

    @property
    def dummy_commit_message(self):
        return f'{DUMMY_COMMIT_SIGNAL} {self.branch_name}'

    @property
    def modified_files(self):
        return [pathlib.Path(item.a_path) for item in self._repo.index.diff(None)]

    @property
    def untracked_files(self):
        return [pathlib.Path(item) for item in self._repo.untracked_files]

    @property
    def branch_name(self):
        return self._repo.active_branch.name

    @property
    def hash(self):
        return self._repo.head.object.hexsha

    def commit_all(self, message):
        self.add_all()
        self.commit(message)

    def ammend_all(self):
        branch = self._repo.head.reference
        commit = self._repo.head.commit
        message = commit.summary

        branch.commit = commit.parents[0]
        self.commit_all(message)

    def add(self, files):
        self._repo.index.add([str(file) for file in files])

    def add_all(self):
        self.add(self.modified_files)
        self.add(self.untracked_files)

    def commit(self, message, skip_hooks=True):
        self._repo.index.commit(message, skip_hooks=skip_hooks)

    def push(self, endpoint):
        # git push -f ssh://vm1:12800/Users/gschrock/code/i95/
        url = f'ssh://{endpoint.host}:{endpoint.port}{self.root}'

        try:
            remote = self._repo.remotes.lookaside
            remote.set_url(url)
        except AttributeError:
            remote = self._repo.create_remote('lookaside', url)

        remote.push(force=True)


def get_root_path():
    return Repo.create().root


def push_to_lookaside(endpoint, repo):
    git_repo = Repo.create()

    if git_repo.is_dirty:
        if git_repo.has_dummy_commit:
            git_repo.ammend_all()
        else:
            git_repo.commit_all(git_repo.dummy_commit_message)

    git_repo.push(endpoint)
    _update_remote_head(endpoint, repo, git_repo)



def _update_remote_head(endpoint, repo, git_repo):
    commands = [
        'git clean -df',
        f'git checkout -f {git_repo.branch_name}',
        'greset --hard HEAD',
        'gmodule update',
        f'git rev-parse HEAD | grep -q {git_repo.hash}'
    ]

    with ssh.connect(endpoint, container=repo.active_container) as connection:
        connection.set_directory(git_repo.root)
        connection.execute_and_print(
            ' && '.join(commands)
        )

#!/usr/local/bin/python3

import build
import click
import commands
import notify
import file_relations
import filters
import git
import os.path
import targets
import test
import time


_FILE_NAME = os.path.expanduser('~/Downloads/tmp.log')


@click.command("file")
@click.argument('file-path', type=click.Path(exists=True))
def open_test_log(file_path):
    """Runs remotely the test log associated with the specified files"""

    unit_test = file_relations.get_unit_test_path(file_path)
    if unit_test is None:
        print('No tests found for ' + file_path)
        exit(0)

    target_info = targets.find_target_info_for(unit_test)
    executable_path = file_relations.get_built_executable_path(
        os.path.join(target_info.directory, target_info.target))
    log_name = executable_path + '.log'

    if log_name in commands.run_remote_command('ls ' + log_name).get_output():
        scp_cmd = 'scp vm1:{} {}'.format(log_name, _FILE_NAME)
        commands.run_local_command(scp_cmd)
        commands.run_local_command('code -r ' + _FILE_NAME)
        time.sleep(1)


if __name__ == '__main__':
    open_test_log()

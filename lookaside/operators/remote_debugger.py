#!/usr/local/bin/python3

# import commands
import notify
import file_command
import file_relations
import filters
import git
import os.path
import targets
import test
import vscode


def setup_for_file(file_path):
    unit_test = file_relations.get_unit_test_path(file_path)
    if unit_test is None:
        print('No unit tests found for ' + file_path)
        exit(0)

    target_info = targets.find_target_info_for(unit_test)
    target_filters = filters.get_filters_for_tests_in(unit_test)

    if target_info is not None:
        try:
            remote_executable_path = file_relations.get_built_executable_path(
                os.path.join(target_info.directory, target_info.target))

            vscode.populate_launch_file_contents(
                target_info.target,
                remote_executable_path,
                target_filters)

        except Exception as e:
            print(e)
    else:
        print("Could not find targets to run")
        notify.notify("Could not find targets to run")


if __name__ == '__main__':
    command = file_command.FileCommand('Setup launch.json to run this file')
    command.fail_if_not_exists()
    setup_for_file(command.get_file_path())

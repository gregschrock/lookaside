import contextlib
import pathlib
import json

import attr
import marshmallow


@attr.s
class Endpoint:
    name = attr.ib()
    host = attr.ib()
    port = attr.ib(default=22)
    dhost = attr.ib(default=True)

    @classmethod
    def create_by_name(cls, name, *, hostname, port=22):
        return cls(hostname, port)

    @classmethod
    def create_by_ip(cls, name, *, ip_address, port=22):
        return cls(ip_address, port)

    def __str__(self):
        return f'{self.host}:{self.port}'


@attr.s
class Repo:
    name = attr.ib()
    path = attr.ib()
    active_container = attr.ib(default=None)

    def contains(self, path):
        return str(path.resolve()).startswith(str(self.path.resolve()))


@attr.s
class Association:
    repo = attr.ib()
    endpoint = attr.ib()


class EndpointSchema(marshmallow.Schema):
    name = marshmallow.fields.Str()
    host = marshmallow.fields.Str()
    port = marshmallow.fields.Integer()
    dhost = marshmallow.fields.Bool(missing=False)

    @marshmallow.post_load
    def create(self, data, **kwargs):
        return Endpoint(**data)


class RepoSchema(marshmallow.Schema):
    name = marshmallow.fields.String()
    path = marshmallow.fields.String()
    active_container = marshmallow.fields.Str(allow_none=True)

    @marshmallow.post_load
    def create(self, data, **kwargs):
        return Repo(data['name'], pathlib.Path(data['path']).resolve(), data.get('active_container', None))


class AssociationSchema(marshmallow.Schema):
    repo = marshmallow.fields.Str()
    endpoint = marshmallow.fields.Str()

    @marshmallow.post_load
    def create(self, data, **kwargs):
        return Association(**data)


class NamedValueStore:

    def __init__(self, schema):
        self.schema = schema
        self._data = dict()

    def add(self, item):
        self._data[item.name] = item

    def remove(self, item_name):
        with contextlib.suppress(KeyError):
            self._data.pop(item_name)

    def get(self, item_name):
        return self._data[item_name]

    def names(self):
        return list(self._data.keys())

    def list(self):
        return list(self._data.values())

    def __len__(self):
        return len(self._data)

    def load(self, path):
        self._data = dict()

        if not path.exists():
            return

        with path.open('r') as data_file:
            data_json = json.load(data_file)
            items = self.schema.load(data_json, many=True)

            self._data = {item.name : item for item in items}

    def save(self, path):
        with path.open('w') as data_file:
            data_json = self.schema.dump(self.list(), many=True)
            json.dump(data_json, data_file)


class Endpoints(NamedValueStore):
    def __init__(self):
        super().__init__(EndpointSchema())


class Repos(NamedValueStore):
    def __init__(self):
        super().__init__(RepoSchema())


@attr.s
class Associations:
    _repo_endpoints = attr.ib(default=attr.Factory(dict))

    def add(self, repo, endpoint):
        self._repo_endpoints[repo] = endpoint

    def remove(self, repo):
        with contextlib.suppress(KeyError):
            self._repo_endpoints.pop(repo)

    def list(self):
        return [
            Association(repo, endpoint)
            for repo, endpoint in self._repo_endpoints.items()
        ]

    def get_endpoint(self, repo):
        return self._repo_endpoints[repo]
    
    def get_repos(self, endpoint):
        return [
            repo
            for repo, endpoint in self._repo_endpoints.items()
            if endpoint == endpoint
        ]

    def __len__(self):
        return len(self._repo_endpoints)

    def load(self, path):
        self._repo_endpoints = dict()

        if not path.exists():
            return

        with path.open('r') as data_file:
            schema = AssociationSchema()
            data_json = json.load(data_file)
            associations = schema.load(data_json, many=True)

            self._repo_endpoints = {
                association.repo : association.endpoint
                for association in associations
            }

    def save(self, path):
        with path.open('w') as data_file:
            schema = AssociationSchema()
            data_json = schema.dump(self.list(), many=True)
            json.dump(data_json, data_file)

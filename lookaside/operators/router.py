import pathlib

import attr

from lookaside.operators import resources


class NoAssociatedRepo(Exception):
    def __init__(self, path):
        super().__init__(f'There is no repo for {path}.')

        self.path = path

class NoSuchRepo(Exception):
    def __init__(self, repo_name):
        super().__init__(f'There is no repo "{repo_name}".')


class NoSuchEndpoint(Exception):
    def __init__(self, endpoint_name):
        super().__init__(f'There is no endpoint "{endpoint_name}".')

        self.endpoint_name = endpoint_name 


class NoAssociatedEndpoint(Exception):
    def __init__(self, repo_name):
        super().__init__(
            f'There is no endpoint associated with repo "{repo_name}".'
        )

        self.repo_name = repo_name


class InvalidAssociationException(Exception):
    def __init__(self, repo_name, endpoint_name):
        super().__init__(
            f'An internal error resulted in the repo "{repo_name}" being '
            f'associated with a non-existent endpoint "{endpoint_name}".'
        )

        self.repo_name = repo_name
        self.endpoint_name = endpoint_name


def _read(decorated):

    def decorator(self, *args, **kwargs):
        self._ensure_resources_loaded()
        return decorated(self, *args, **kwargs)

    return decorator


def _write(decorated):

    def decorator(self, *args, **kwargs):
        result = decorated(self, *args, **kwargs)
        self._save_resources()

        return result

    return decorator


def _read_write(decorated):

    def decorator(self, *args, **kwargs):
        self._ensure_resources_loaded()
        result = decorated(self, *args, **kwargs)
        self._save_resources()

        return result
    
    return decorator


@attr.s
class Router:
    config_path = attr.ib()
    _endpoints = attr.ib(attr.Factory(resources.Endpoints))
    _repos = attr.ib(attr.Factory(resources.Repos))
    _associations = attr.ib(attr.Factory(resources.Associations))
    _loaded = False

    @_write
    def create_repo(self, name, path, active_container=None):
        self._repos.add(resources.Repo(name, pathlib.Path(path), active_container))

    @_write
    def create_endpoint(self, name, host, port, dhost):
        self._endpoints.add(resources.Endpoint(name, host, port, dhost))

    @_read
    def endpoints(self):
        return self._endpoints.list()

    @_read
    def repos(self):
        return self._repos.list()

    @_read
    def endpoint_names(self):
        return self._endpoints.names()

    @_read
    def repo_names(self):
        return self._repos.names()

    @_read
    def associations(self):
        return self._associations.list()

    @_read_write
    def delete_repo(self, name):
        self._repos.remove(name)

    @_write
    def delete_endpoint(self, name):
        self._endpoints.remove(name)

    @_write
    def associate(self, repo_name, endpoint_name):
        if not repo_name in self.repo_names():
            raise NoSuchRepo(repo_name)

        if not endpoint_name in self.endpoint_names():
            raise NoSuchEndpoint(endpoint_name)

        self._associations.add(repo_name, endpoint_name)

    @_read_write
    def disassociate(self, repo_name):
        self._associations.remove(repo_name)

    @_read
    def get_repo_endpoint_name(self, repo_name):
        try:
            return self._associations.get_endpoint(repo_name)
        except KeyError as e:
            raise NoAssociatedEndpoint(repo_name) from e

    @_read
    def get_repo_endpoint(self, repo_name):
        endpoint_name = self.get_repo_endpoint_name(repo_name)

        try:
            return self._endpoints.get(endpoint_name)
        except KeyError as e:
            raise InvalidAssociationException(repo_name, endpoint_name) from e

    @_read
    def get_endpoint_for_path(self, path):
        repo_name = self.get_repo_for_path(path).name
        return self.get_repo_endpoint(repo_name)

    @_read
    def get_repo_for_path(self, path):
        for repo in self._repos.list():
            if repo.contains(pathlib.Path(path)):
                return repo
        raise NoAssociatedRepo(path)

    @_read
    def get_repos_referencing_endpoint(self, endpoint_name):
        return self._associations.get_repos(endpoint_name)

    def _ensure_resources_loaded(self):
        if not self._loaded:
            self._load_resources()
            self._loaded = True

    def _load_resources(self):
        if self.config_path.exists():
            self._endpoints.load(self.config_path / 'endpoints.json')
            self._repos.load(self.config_path / 'repos.json')
            self._associations.load(self.config_path / 'repo-endpoints.json')

    def _save_resources(self):
        self.config_path.mkdir(parents=True, exist_ok=True)
        self._endpoints.save(self.config_path / 'endpoints.json')
        self._repos.save(self.config_path / 'repos.json')
        self._associations.save(self.config_path / 'repo-endpoints.json')

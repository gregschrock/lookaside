#!/usr/local/bin/python3

import file_command

import argparse
import os


class SymbolFactory:
    def __init__(self):
        self.open_list = ['{', '(', '[']
        self.close_lsit = ['}', ')', ']']

    def is_symbol(self, symbol):
        return self.is_opening_symbol(symbol) or self.is_closing_symbol(symbol)

    def is_opening_symbol(self, symbol):
        return symbol in self.open_list

    def is_closing_symbol(self, symbol):
        return symbol in self.close_lsit

    def is_closing_symbol_for(self, symbol, opening):
        return self.get_opening_for(symbol) == opening

    def is_opening_symbol_for(self, symbol, closing):
        return self.get_closing_for(symbol) == closing

    def get_closing_for(self, symbol):
        return self.close_lsit[self.open_list.index(symbol)]

    def get_opening_for(self, symbol):
        return self.open_list[self.close_lsit.index(symbol)]


class Symbol:
    def __init__(self, value, line_index, char_index, file_path):
        self.value = value
        self.line_index = line_index
        self.char_index = char_index
        self.file_path = file_path

    def __str__(self):
        return '"' + self.value + '" at ' + str(self.line_index) + ':' + str(self.char_index) + ' in ' + self.file_path

    def __repr__(self):
        return str(self)


class SymbolMatcher:
    def __init__(self, content, file_path):
        self._content = content
        self._file_path = file_path
        self._symbol_factory = SymbolFactory()
        self._symbol_stack = []
        self._line_index = 1
        self._char_index = 1

    def validate_symbols(self):
        for self._line_index, line in enumerate(self._content.splitlines()):
            self._char_index = 1
            for self._char_index, char in enumerate(line):
                if self._symbol_factory.is_symbol(char):
                    self._handle_symbol(char)

        if self._has_symbols():
            print("No closing symbol for " + str(self._symbol_stack.pop()))

    def _handle_symbol(self, symbol):
        if self._symbol_factory.is_opening_symbol(symbol):
            self._handle_opening_symbol(symbol)
        else:
            self._handle_closing_symbol(symbol)

    def _handle_opening_symbol(self, symbol):
        self._symbol_stack.append(self._create_symbol(symbol))

    def _handle_closing_symbol(self, symbol):
        target_closing_symbol = self._create_symbol(symbol)

        if not self._has_symbols():
            raise(UnexpectedClosingSymbol(target_closing_symbol))

        target_opening_symbol = self._symbol_stack.pop()
        if not self._symbol_factory.is_closing_symbol_for(
                target_closing_symbol.value,
                target_opening_symbol.value):
            raise MismatchedClosingSymbol(
                target_opening_symbol, target_closing_symbol)

    def _create_symbol(self, symbol):
        return Symbol(symbol, self._line_index + 1, self._char_index + 1, self._file_path)

    def _has_symbols(self):
        return not len(self._symbol_stack) == 0


class SymbolError(Exception):
    pass


class UnexpectedClosingSymbol(SymbolError):
    def __init__(self, symbol):
        message = 'Unexpected closing symbol ' + str(symbol)
        super(UnexpectedClosingSymbol, self).__init__(message)


class MismatchedClosingSymbol(SymbolError):
    def __init__(self, opening_symbol, closing_symbol):
        message = ('Unexpected closing symbol ' + str(closing_symbol) +
                   ' for opening symbol ' + str(opening_symbol))
        super(MismatchedClosingSymbol, self).__init__(message)


class UnterminatedOpeningSymbol(SymbolError):
    def __init__(self, symbol):
        message = 'Unterminated opening symbol ' + str(symbol)
        super(UnexpectedClosingSymbol, self).__init__(message)


class MatcherIssue:
    def __init__(self, line_number, char_number, description):
        self.line_number = line_number
        self.char_number = char_number
        self.description = description


def run_with_content(content, file_path):
    matcher = SymbolMatcher(content, file_path)
    try:
        matcher.validate_symbols()
    except SymbolError as e:
        print(e)
    else:
        print('Everything looks good in ' + file_path)


# if __name__ == '__main__':
#     run_with_content('{}')
#     run_with_content('()')
#     run_with_content('{')
#     run_with_content('(')
#     run_with_content('}')
#     run_with_content(')')

if __name__ == '__main__':
    command = file_command.FileCommand('Validate balance in c++ structures')
    run_with_content(command.get_file_contents(), command.get_file_path())

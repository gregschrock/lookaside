import pathlib


def run_targets(connection, targets, *, filter=None, valgrind=True):
    target_list = ' '.join(targets)
    filter_string = f'-f "{filter}" ' if filter else ''
    valgrind_string = '-V ' if not valgrind else ''
    run_string = f'-run {target_list}' if len(target_list) > 0 else ''

    command = f'validate -c {valgrind_string}{filter_string}{run_string}'

    print('executing:', command)
    return connection.execute_and_print(command)

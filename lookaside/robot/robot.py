import collections
import itertools
import pathlib
import subprocess
import shutil
import attr


@attr.s
class Flag:
    translation = attr.ib()
    raw_robot_option = attr.ib(default=False)


_FLAG_TRANSLATION = {
    'force_install': Flag('-fi'),
    'skip_config': Flag('-sc'),
    'skip_install': Flag('-si'),
    'skip_setup': Flag('-ss'),
    'skip_teardown': Flag('-st'),
    'recreate_testbed': Flag('-rc'),
    'teardown_testbed': Flag('-tt'),
    'skip_clean': Flag('-v SKIP_INITIAL_CLEANUP:True', True)
}


_FLAG_MAP = collections.OrderedDict([
    ('nuke', ['teardown_testbed', 'skip_teardown']),
    ('install', ['skip_teardown', 'skip_clean']),
    ('reinstall', ['skip_teardown']),
    ('setup', ['skip_install', 'skip_teardown']),
    ('config', ['skip_install', 'skip_setup', 'skip_teardown']),
    ('run', ['skip_install', 'skip_setup', 'skip_config', 'skip_teardown']),
    ('teardown', ['skip_install', 'skip_setup', 'skip_config']),
    ('full', [])
])


# _REMOTE_OUTDIR = 'lookaside-robot-output'


def get_run_options():
    return _FLAG_MAP.keys()


def get_build_options():
    return ['debug', 'release']


def run(connection, *, outdir, run_type, build_type, testbed, suite, robot_variables=[], randomize=False, extras=[]):
    flags = _get_robot_flags(run_type, build_type, robot_variables, randomize, extras)
    robot_command = _get_robot_command(flags, testbed, suite, outdir)

    print(robot_command)
    return connection.execute_and_print(robot_command)


def retrieve_results(connection, remote_out, local_out):
    # destination = pathlib.Path(outdir).resolve()
    # shutil.rmtree(outdir, ignore_errors=True)

    # remote_dir = pathlib.Path(connection.cwd) / _REMOTE_OUTDIR
    # source = f'{connection.endpoint.host}:{remote_dir}'

    # subprocess.run([
    #     'scp', '-r', '-P',  connection.endpoint.port,
    #     source,
    #     destination
    # ])

    return connection.get_dir(remote_out, local_out)


def open_results(outdir, suite):
    return subprocess.run(
        ['open', pathlib.Path.cwd() / outdir / f'log{suite.stem}.html']
    )


def _get_robot_flags(run_type, build_type, robot_variables, randomize, extras):
    return (
        ['--build_type', build_type] +
        list(_get_robot_flags_subset(run_type)) +
        list(_get_raw_robot_flags(run_type, randomize)) +
        _get_run_robot_variables(robot_variables) +
        _get_run_robot_extras(extras)
    )


def _get_raw_robot_flags(run_type, randomize):
    raw_robot_flags = list(_get_robot_flags_subset(run_type, raw=True))

    if randomize:
        raw_robot_flags += ["--randomize", "tests"]

    if len(raw_robot_flags) == 0:
        return []

    raw_flags = ' '.join(raw_robot_flags)
    return [f'--robot_options="{raw_flags}"']


def _get_robot_flags_subset(run_type, raw=False):
    return {
        _FLAG_TRANSLATION[flag].translation
        for flag in _FLAG_MAP[run_type]
        if _FLAG_TRANSLATION[flag].raw_robot_option == raw
    }


def _get_robot_command(flags, testbed, suite, outdir):
    flags = ' '.join(flags)

    return '&&'.join([
        f'rm -rf {outdir}',
        f'mkdir -p {outdir}',
        f'run_robot --outputdir={outdir} {flags} T{testbed} {suite}'
    ])


def _get_run_robot_variables(robot_variables):
    return list(itertools.chain(*[("-v", variable) for variable in robot_variables]))


def _get_run_robot_extras(extras):
    return list(itertools.chain.from_iterable(extras))

#!/usr/local/bin/python3

import pathlib

import file_writer
import git

_launch_template = """
{{
    "version": "0.2.0",
    "configurations": [
        {{
            "type": "cppdbg",
            "request": "launch",
            "name": "{executable_name}",
            "args": [
                "--gtest_filter={filter_string}"
            ],
            "program": "{executable_path}",
            "internalConsoleOptions": "openOnSessionStart",
            "cwd": "${{workspaceRoot}}",
            "pipeTransport": {{
                "pipeCwd": "/usr/bin",
                "pipeProgram": "/usr/bin/ssh",
                "pipeArgs": [
                    "vm1"
                ],
                "debuggerPath": "/usr/bin/gdb"
            }}
        }}
    ]
}}"""


_task_json_template = """
{{
    // See https://go.microsoft.com/fwlink/?LinkId=733558
    // for the documentation about the tasks.json format
    "version": "2.0.0",
    "tasks": [
        {task_list}
    ]
}}
"""


_file_task_template = """
        {{
            "label": "{command}",
            "type": "shell",
            "command": "{command} ${{file}}",
            "group": "build",
            "presentation": {{
                "reveal": "always"
            }},
            "problemMatcher": []
        }}"""


_file_commands = [
    'find_mismatched',
    'rbuild_executable',
    'ropen_test_log',
    'lookaside test file',
    'lookaside test executable',
    # 'rtest_executable',
    # 'rtest_file',
    'setup_lookaside_debug',
]


def populate_task_file_contents():
    task_list = ','.join(_file_task_template.format(
        command=command) for command in _file_commands)
    tasks_json = _task_json_template.format(task_list=task_list)

    # print(tasks_json)
    with open(get_tasks_file_path(), 'w') as tasks_file:
        tasks_file.write(tasks_json)


def populate_launch_file_contents(executable_name, executable_path, filter_string):
    file_path = get_launch_file_path()

    contents = get_launch_file_contents(
        executable_name,
        executable_path,
        filter_string
    )

    print(file_path)
    print(contents)

    file_writer.write_file(file_path, contents)


def get_launch_file_contents(executable_name, executable_path, filter_string):
    return _launch_template.format(
        executable_name=executable_name,
        executable_path=executable_path,
        filter_string=filter_string)


def get_launch_file_path():
    return pathlib.Path(git.get_root_path()) / '.vscode' / 'launch.json'


def get_tasks_file_path():
    return pathlib.Path(git.get_root_path()) / '.vscode' / 'tasks.json'

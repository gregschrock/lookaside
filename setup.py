import pathlib

from setuptools import find_packages, setup

try:
    with pathlib.Path('requirements.txt').open() as requirements_file:
        requirements = requirements_file.readlines()
except FileNotFoundError:
    requirements = []


setup(
    name='lookaside',
    version='0.2.0',
    description='128 Technology lookaside Execution',
    entry_points={
        'console_scripts': ['lookaside=lookaside.cli.lookaside:lookaside']
    },
    packages=find_packages(),
    package_date={},
    python_requires='>=3.6',
    install_requires=requirements
)

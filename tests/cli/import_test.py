import sys

import click.testing
import pytest

from lookaside.cli import lookaside


def test_help():
    runner = click.testing.CliRunner()

    print(runner.invoke(
        lookaside.lookaside,
        ['--help'],
        catch_exceptions=False,
    ).output)


if __name__ == '__main__':
    sys.exit(pytest.main([__file__, '-vvv']))
import pathlib
import sys

import click.testing
import pytest

from lookaside.cli import lookaside
from lookaside.clients import command_results
from lookaside.clients import local
from lookaside.codebase import file_relations
from lookaside.gtest import filter as gtest_filter

_ACTUAL_CWD = pathlib.Path.cwd()


@pytest.fixture
def mock_rpush(mocker):
    return mocker.patch('lookaside.operators.git.push_to_lookaside')


@pytest.fixture
def mock_ssh(mocker):
    mock = mocker.MagicMock(spec=local.Client)
    connect_mock = mocker.patch('lookaside.clients.ssh.connect')
    connect_mock.return_value.__enter__.return_value = mock

    return mock


@pytest.fixture
def call(mocker):
    return mocker.call


class ClickRunner:

    @staticmethod
    def invoke_cmd_with(command, *args, **kwargs):
        runner = click.testing.CliRunner()
        context_obj = lookaside.CmdContext(endpoint=0)

        return runner.invoke(
            command,
            args,
            obj=context_obj,
            catch_exceptions=False,
            **kwargs
        )

    def invoke_with(self, *args, expect_fail=False, **kwargs):
        result = self.invoke_cmd_with(
            self.get_click_command(), *args, **kwargs)

        if not expect_fail and result.exit_code != 0:
            print(result.output)
            assert result.exit_code == 0

        return result

    def invoke(self, expect_fail=False):
        return self.invoke_with(expect_fail=expect_fail)

    def get_click_command(self):
        pass


@pytest.mark.usefixtures('mock_rpush', 'mock_ssh', 'fs')
class TestEndpointContext(ClickRunner):

    def get_click_command(self):
        return lookaside.lookaside

    def test_fails_without_context(self):
        result = self.invoke_with('build', 'rpm', expect_fail=True)

        assert result.output == (
            f'There is no endpoint associated with "{_ACTUAL_CWD}". '
            'Please create one and try again.\n'
        )
        assert result.exit_code != 0

    def test_succeeds_with_context(self):
        self.invoke_with('create', 'repo', '--name', 'test_repo')

        self.invoke_with(
            'create', 'endpoint',
            '--name', 'test_endpoint',
            '--host', 'test_host',
            '--port', '22'
        )

        self.invoke_with(
            'create', 'association',
            '--repo', 'test_repo',
            '--endpoint', 'test_endpoint'
        )

        self.invoke_with('build', 'rpm')


@pytest.mark.usefixtures('mock_rpush', 'mock_ssh')
class TestBuild(ClickRunner):

    def get_click_command(self):
        return lookaside.lookaside_build

    def test_pushes_during_build(self, mock_rpush):
        self.invoke()
        mock_rpush.assert_called_once()

    def test_changes_to_current_directory(self, mock_ssh):
        self.invoke()
        mock_ssh.set_directory_to_cwd.assert_called_once()

    def test_builds_target_with_no_flags(self, mock_ssh):
        self.invoke_with('a_target')
        mock_ssh.execute_and_print.assert_called_once_with('build a_target')

    def test_builds_target_with_unity(self, mock_ssh):
        self.invoke_with('a_target', '--unity')
        mock_ssh.execute_and_print.assert_called_once_with(
            'build a_target_unity')

    def test_builds_target_without_unity(self, mock_ssh):
        self.invoke_with('a_target', '--no-unity')
        mock_ssh.execute_and_print.assert_called_once_with('build a_target')

    def test_builds_target_with_clang(self, mock_ssh):
        self.invoke_with('a_target', '-c')
        mock_ssh.execute_and_print.assert_called_once_with(
            'build -clang a_target'
        )

    def test_builds_multiple_targets(self, mock_ssh):
        self.invoke_with('a_target', 'another_target')
        mock_ssh.execute_and_print.assert_called_once_with(
            'build a_target another_target'
        )


@pytest.mark.usefixtures('mock_rpush', 'mock_ssh')
class TestValidate(ClickRunner):

    def get_click_command(self):
        return lookaside.lookaside_validate

    def test_pushes_during_build(self, mock_rpush):
        self.invoke()
        mock_rpush.assert_called_once()

    def test_changes_to_current_directory_for_build_and_test(self, mock_ssh):
        self.invoke()
        assert mock_ssh.set_directory_to_cwd.call_count == 2

    def test_builds_target_with_no_flags(self, mock_ssh, call):
        self.invoke_with('a_target')
        mock_ssh.execute_and_print.assert_has_calls([call('build a_target')])

    def test_builds_target_with_unity(self, mock_ssh, call):
        self.invoke_with('a_target', '--unity')
        mock_ssh.execute_and_print.assert_has_calls(
            [call('build a_target_unity')]
        )

    def test_builds_target_without_unity(self, mock_ssh, call):
        self.invoke_with('a_target', '--no-unity')
        mock_ssh.execute_and_print.assert_has_calls([call('build a_target')])

    def test_builds_target_with_clang(self, mock_ssh, call):
        self.invoke_with('a_target', '-c')
        mock_ssh.execute_and_print.assert_has_calls(
            [call('build -clang a_target')]
        )

    def test_builds_multiple_targets(self, mock_ssh, call):
        self.invoke_with('a_target', 'another_target')
        mock_ssh.execute_and_print.assert_has_calls(
            [call('build a_target another_target')]
        )

    def test_runs_cpp_validate_on_target(self, mock_ssh, call):
        self.invoke_with('a_target')
        mock_ssh.execute_and_print.assert_has_calls(
            [call('validate -c -V -run a_target')]
        )

    def test_runs_cpp_validate_on_multiple_targets(self, mock_ssh, call):
        self.invoke_with('a_target', 'another_target')
        mock_ssh.execute_and_print.assert_has_calls(
            [call('validate -c -V -run a_target another_target')]
        )

    def test_skips_build(self, mock_ssh):
        self.invoke_with('a_target', '--skip-build')
        mock_ssh.execute_and_print.assert_called_once_with(
            'validate -c -V -run a_target'
        )

    def test_includes_test_filter(self, mock_ssh, call):
        self.invoke_with('a_target', '-f', 'some-filter')
        mock_ssh.execute_and_print.assert_has_calls(
            [call('validate -c -V -f "some-filter" -run a_target')]
        )

    def test_runs_with_valgrind(self, mock_ssh, call):
        self.invoke_with('a_target', '--valgrind')
        mock_ssh.execute_and_print.assert_has_calls(
            [call('validate -c -run a_target')]
        )

    def test_runs_with_no_test_if_none_specified(self, mock_ssh, call):
        self.invoke_with()
        mock_ssh.execute_and_print.assert_has_calls(
            [call('validate -c')]
        )

    def test_does_not_test_if_build_fails(self, mock_ssh, call, mocker):
        def on_execute(command):
            mockresult = mocker.MagicMock(spec=command_results.CommandResults)
            was_successful = False if 'build' in command else True
            mockresult.was_successful.return_value = was_successful

            return mockresult

        mock_ssh.execute_and_print.side_effect = on_execute

        self.invoke_with('a_target')
        assert mock_ssh.execute_and_print.call_count == 1


@pytest.mark.usefixtures('mock_rpush', 'mock_ssh')
class TestFileValidate(ClickRunner):
    example_file = '/Users/gschrock/code/i95/src/lib/queue/TankClient.hpp'
    example_target = 'libQueueTest'

    def get_expected_file_only_filter(self):
        path = pathlib.Path(self.example_file)
        unit_test = file_relations.get_unit_test_path(path)

        return gtest_filter.get_filters_for_tests_in(unit_test)

    def get_click_command(self):
        return lookaside.lookaside_file_validate

    def test_pushes_during_build(self, mock_rpush):
        self.invoke_with(self.example_file)
        mock_rpush.assert_called_once()

    def test_changes_to_current_directory_for_build_and_test(self, mock_ssh):
        self.invoke_with(self.example_file)
        assert mock_ssh.set_directory_to_cwd.call_count == 2

    def test_builds_target_with_no_flags(self, mock_ssh, call):
        self.invoke_with(self.example_file)
        mock_ssh.execute_and_print.assert_has_calls([
            call(f'build {self.example_target}')
        ])

    def test_builds_target_with_unity(self, mock_ssh, call):
        self.invoke_with(self.example_file, '--unity')
        mock_ssh.execute_and_print.assert_has_calls([
            call(f'build {self.example_target}_unity')
        ])

    def test_builds_target_without_unity(self, mock_ssh, call):
        self.invoke_with(self.example_file, '--no-unity')
        mock_ssh.execute_and_print.assert_has_calls([
            call(f'build {self.example_target}')
        ])

    def test_builds_target_with_clang(self, mock_ssh, call):
        self.invoke_with(self.example_file, '-c')
        mock_ssh.execute_and_print.assert_has_calls([
            call(f'build -clang {self.example_target}')
        ])

    def test_runs_cpp_validate_on_target(self, mock_ssh, call):
        self.invoke_with(self.example_file)
        mock_ssh.execute_and_print.assert_has_calls(
            [call(f'validate -c -V -run {self.example_target}')]
        )

    def test_skips_build(self, mock_ssh):
        self.invoke_with(self.example_file, '--skip-build')
        mock_ssh.execute_and_print.assert_called_once_with(
            f'validate -c -V -run {self.example_target}'
        )

    def test_includes_test_filter(self, mock_ssh, call):
        self.invoke_with(self.example_file, '-f', 'some-filter')
        mock_ssh.execute_and_print.assert_has_calls([
            call(f'validate -c -V -f "some-filter" -run {self.example_target}')
        ])

    def test_filters_within_file(self, mock_ssh, call):
        self.invoke_with(self.example_file, '--file-only')

        mock_ssh.execute_and_print.assert_has_calls([
            call(
                f'validate -c -V -f "{self.get_expected_file_only_filter()}" '
                f'-run {self.example_target}'
            )
        ])

    def test_runs_with_valgrind(self, mock_ssh, call):
        self.invoke_with(self.example_file, '--valgrind')
        mock_ssh.execute_and_print.assert_has_calls([
            call(f'validate -c -run {self.example_target}')
        ])

    def test_does_not_test_if_build_fails(self, mock_ssh, call, mocker):
        def on_execute(command):
            mockresult = mocker.MagicMock(spec=command_results.CommandResults)
            was_successful = False if 'build' in command else True
            mockresult.was_successful.return_value = was_successful

            return mockresult

        mock_ssh.execute_and_print.side_effect = on_execute

        self.invoke_with(self.example_file)
        assert mock_ssh.execute_and_print.call_count == 1


class TestResourceCreation(ClickRunner):

    def get_click_command(self):
        return lookaside.lookaside

    def test_creates_repos(self, fs):
        self.invoke_with(
            'create', 'repo',
            '--name', 'test_repo',
            '--path', '.'
        )

        result = self.invoke_with('show', 'repos')
        assert result.output == (
            'Name       Path\n'
            '---------  ------\n'
            'test_repo  .\n'
        )

    def test_creates_endpoints(self, fs):
        self.invoke_with(
            'create', 'endpoint',
            '--name', 'dev_vm',
            '--host', 'vm1',
            '--port', '12800'
        )

        result = self.invoke_with('show', 'endpoints')
        assert result.output == (
            'Name    Host      Port\n'
            '------  ------  ------\n'
            'dev_vm  vm1      12800\n'
        )

    def test_creates_associations(self, fs):
        self.invoke_with(
            'create', 'repo',
            '--name', 'test_repo',
            '--path', '.'
        )

        self.invoke_with(
            'create', 'endpoint',
            '--name', 'dev_vm',
            '--host', 'vm1',
            '--port', '12800'
        )

        self.invoke_with(
            'create', 'association',
            '--repo', 'test_repo',
            '--endpoint', 'dev_vm'
        )

        result = self.invoke_with('show', 'associations')
        assert result.output == (
            'Repo       Endpoint\n'
            '---------  ----------\n'
            'test_repo  dev_vm\n'
        )


class TestFileDescribe(ClickRunner):

    def get_click_command(self):
        return lookaside.describe

    def test_describes_header(self):
        header = pathlib.Path(
            '/Users/gschrock/code/i95/src/lib/queue/TankClient.hpp')
        result = self.invoke_with(str(header))

        assert result.output == (
            '-----------  --------------------------------------------------------------\n'
            'File         /Users/gschrock/code/i95/src/lib/queue/TankClient.hpp\n'
            'Unit Test    /Users/gschrock/code/i95/src/lib/queue/test/TankClientTest.cpp\n'
            'Target       128T_queue\n'
            'Test Target  libQueueTest\n'
            '-----------  --------------------------------------------------------------\n'
        )

    def test_describes_source(self):
        source = pathlib.Path(
            '/Users/gschrock/code/i95/src/lib/queue/TankClient.cpp')
        result = self.invoke_with(str(source))

        assert result.output == (
            '-----------  --------------------------------------------------------------\n'
            'File         /Users/gschrock/code/i95/src/lib/queue/TankClient.cpp\n'
            'Unit Test    /Users/gschrock/code/i95/src/lib/queue/test/TankClientTest.cpp\n'
            'Target       128T_queue\n'
            'Test Target  libQueueTest\n'
            '-----------  --------------------------------------------------------------\n'
        )

    def test_only_displays_file_when_no_test_or_target(self):
        header_only = pathlib.Path(
            '/Users/gschrock/code/i95/src/lib/queue/Log.hpp')
        result = self.invoke_with(str(header_only))

        assert result.output == (
            '----  ----------------------------------------------\n'
            'File  /Users/gschrock/code/i95/src/lib/queue/Log.hpp\n'
            '----  ----------------------------------------------\n'
        )


if __name__ == '__main__':
    sys.exit(pytest.main([__file__, '-vvv']))
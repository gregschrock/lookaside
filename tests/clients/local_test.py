import os
import subprocess

import pytest

from lookaside.clients import command_results
from lookaside.clients import local


def execute(args):
    output = subprocess.Popen(
        args,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE
    )

    return command_results.CommandResults.create(
        command=' '.join(args),
        stdout=output.stdout,
        stderr=output.stderr
    )


class TestClient:

    @pytest.mark.parametrize('path_info', [
        {'path': '~', 'exists': True},
        {'path': '~/this/path/does/not/exist', 'exists': False}
    ])
    def test_ls_commands(self, path_info):
        local_client = local.Client.create()

        command_args = ['ls', os.path.expanduser(path_info['path'])]
        command = ' '.join(command_args)

        client_output = local_client.execute_command(command)
        local_output = execute(command_args)

        if path_info['exists']:
            assert client_output.stdout.full_output == local_output.stdout.full_output
        else:
            assert client_output.stderr.full_output == local_output.stderr.full_output

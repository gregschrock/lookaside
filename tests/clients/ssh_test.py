# Copyright 2018 128 Technology, Inc.

"""Unit tests for the rest module"""

import os.path
import subprocess

import pytest

from lookaside.clients import ssh
from lookaside.clients import command_results
from lookaside.operators import resources


@pytest.fixture
def local_endpoint():
    return resources.Endpoint('127.0.0.1')


@pytest.fixture
def local_client(local_endpoint):
    with ssh.connect(local_endpoint, ssh.Auth.from_keys()) as connection:
        yield connection


def execute(args):
    output = subprocess.Popen(
        args,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE
    )

    return command_results.CommandResults.create(
        command=' '.join(args),
        stdout=output.stdout,
        stderr=output.stderr
    )


class TestAuth:

    def test_keys(self):
        auth = ssh.Auth.from_keys()
        assert auth.connect_kwargs == {}

    def test_password(self):
        auth = ssh.Auth.from_password(username='user', password='password')

        assert auth.connect_kwargs == {
            'username': 'user',
            'password': 'password'
        }


class TestClient:

    @pytest.mark.parametrize('path_info', [
        {'path': '~', 'exists': True},
        {'path': '~/this/path/does/not/exist', 'exists': False}
    ])
    def test_ls_commands(self, local_client, path_info):
        command_args = ['ls', os.path.expanduser(path_info['path'])]
        command = ' '.join(command_args)

        client_output = local_client.execute_command(command)
        local_output = execute(command_args)

        if path_info['exists']:
            assert client_output.stdout.full_output == local_output.stdout.full_output
        else:
            assert client_output.stderr.full_output == local_output.stderr.full_output

    def test_default_login(self, local_endpoint, mocker):
        connect_mock = mocker.patch('paramiko.SSHClient.connect')

        with ssh.connect(local_endpoint) as _:
            pass

        connect_mock.assert_called_once_with('127.0.0.1', 22)

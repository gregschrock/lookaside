import pathlib

import pytest

from lookaside.codebase import cmake


SRC_DIRECOTRY = pathlib.Path.home() / 'code' / 'dev' / 'src'
LIB_DIRECOTRY = SRC_DIRECOTRY / 'lib'
QUEUE_DIRECTORY = LIB_DIRECOTRY / 'queue'


@pytest.fixture
def src_directory():
    return SRC_DIRECOTRY


@pytest.fixture
def queue_directory(src_directory):
    return QUEUE_DIRECTORY


class TestCmakeLists:

    @pytest.fixture
    def queue_cmake(self, queue_directory):
        return cmake.CMakeLists.create_from_directory(queue_directory)

    @pytest.mark.parametrize('src_name', [
        'TankClient.cpp',
        'TopicConfig.cpp'
    ])
    def test_finds_target_that_exists(self, queue_cmake, src_name):
        print(queue_cmake.path)
        assert queue_cmake.exists()
        queue_cmake.load_contents()

        assert queue_cmake.contains_target(src_name)

        target = queue_cmake.get_target(src_name)
        assert target.name == '128T_queue'
        assert target._type == cmake.TargetTypes.LIBRARY

    @pytest.mark.parametrize('src_name', [
        'Nothing.cpp',
        'Everything.cpp',
    ])
    def test_targets_that_dont_exist(self, queue_cmake, src_name):
        assert queue_cmake.exists()
        queue_cmake.load_contents()

        assert not queue_cmake.contains_target(src_name)

        with pytest.raises(cmake.NoSuchTargetError):
            queue_cmake.get_target(src_name)

    def test_targets_from_subdirectories(self, queue_cmake, src_directory):
        lib_cmake = queue_cmake.get_parent_cmake()

        assert lib_cmake.path == src_directory / 'lib' / 'CMakeLists.txt'
        assert lib_cmake.exists()


class TestTarget:

    @pytest.mark.parametrize('file,expected_target', [
        (QUEUE_DIRECTORY / 'TankClient.cpp', '128T_queue'),
        (QUEUE_DIRECTORY / 'test' / 'TopicConfigTest.cpp', 'libQueueTest')
    ])
    def test_valid_targets(self, file, expected_target):
        target = cmake.Target.create_for_file(file)
        assert target.name == expected_target

    @pytest.mark.parametrize('file', [(QUEUE_DIRECTORY / 'DoesNotExist.cpp')])
    def test_invalid_targets(self, file):
        with pytest.raises(FileNotFoundError):
            cmake.Target.create_for_file(file)

    def test_unity(self):
        target = cmake.Target('some_target')

        assert target.unity == 'some_target_unity'
        assert target.non_unity == 'some_target'

    def test_finds_static_library(self):
        path = (
            SRC_DIRECOTRY / 'apps' / 'PersistentDataManager' /
            'rpc' / 'GetEntitlementUtilizedRpcHandler.cpp'
        )

        target = cmake.Target.create_for_file(path)
        assert target.name == '128T_persistentDataManager'



class TestTestTargets:

    @pytest.mark.parametrize('file,expected_target', [
        (QUEUE_DIRECTORY / 'TankClient.cpp', 'libQueueTest'),
        (QUEUE_DIRECTORY / 'test' / 'TopicConfigTest.cpp', 'libQueueTest')
    ])
    def test_valid_targets(self, file, expected_target):
        target = cmake.TestTarget.create_for_file(file)
        assert target.name == expected_target

    @pytest.mark.parametrize('file', [(QUEUE_DIRECTORY / 'DoesNotExist.cpp')])
    def test_invalid_targets(self, file):
        with pytest.raises(FileNotFoundError):
            cmake.TestTarget.create_for_file(file)

    def test_unity(self):
        target = cmake.TestTarget('some_target')

        assert target.unity == 'some_target_unity'
        assert target.non_unity == 'some_target'


class TestTargets:

    @pytest.fixture
    def some_targets(self):
        return cmake.Targets([
            cmake.Target('t1'),
            cmake.Target('t2'),
            cmake.Target('t3')
        ])

    def test_unity_list(self, some_targets):
        assert some_targets.as_unity() == ['t1_unity', 't2_unity', 't3_unity']

    def test_non_unity_list(self, some_targets):
        assert some_targets.as_non_unity() == ['t1', 't2', 't3']

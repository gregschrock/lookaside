import pathlib

import pytest

from lookaside.codebase import file_relations


SRC_DIRECOTRY = pathlib.Path.home() / 'code' / 'dev' / 'src'
LIB_DIRECOTRY = SRC_DIRECOTRY / 'lib'
QUEUE_DIRECTORY = LIB_DIRECOTRY / 'queue'
TEST_DIRECTORY = QUEUE_DIRECTORY / 'test'

HPP_PATH = QUEUE_DIRECTORY / 'TankClient.hpp'
CPP_PATH = QUEUE_DIRECTORY / 'TankClient.cpp'
TEST_PATH = TEST_DIRECTORY / 'TankClientTest.cpp'

TARGET = '128T_queue'
TEST_TARGET = 'libQueueTest'


@pytest.mark.parametrize('path,is_test', [
    (HPP_PATH, False),
    (CPP_PATH, False),
    (QUEUE_DIRECTORY, False),
    (TEST_DIRECTORY, False),
    (TEST_PATH, True),
])
def test_that_path_is_test(path, is_test):
    assert file_relations.is_test(path) == is_test


@pytest.mark.parametrize('path,is_test_directory', [
    (HPP_PATH, False),
    (CPP_PATH, False),
    (QUEUE_DIRECTORY, False),
    (TEST_DIRECTORY, True),
    (TEST_PATH, False),
])
def test_that_path_is_test_directory(path, is_test_directory):
    assert file_relations.is_test_directory(path) == is_test_directory


@pytest.mark.parametrize('path,is_in_test_directory', [
    (HPP_PATH, False),
    (CPP_PATH, False),
    (QUEUE_DIRECTORY, False),
    (TEST_DIRECTORY, True),
    (TEST_PATH, True),
])
def test_that_path_in_test_directory(path, is_in_test_directory):
    assert file_relations.is_in_test_directory(path) == is_in_test_directory


@pytest.mark.parametrize('path', [HPP_PATH, CPP_PATH, TEST_PATH])
def test_finds_valid_unit_test_path(path):
    assert file_relations.get_unit_test_path(path) == TEST_PATH


@pytest.mark.parametrize('path', [QUEUE_DIRECTORY, TEST_DIRECTORY])
def test_raises_not_found_for_paths_without_unit_tests(path):
    with pytest.raises(FileNotFoundError):
        file_relations.get_unit_test_path(path)


@pytest.mark.parametrize('path', [
    HPP_PATH,
    CPP_PATH,
    QUEUE_DIRECTORY,
    TEST_DIRECTORY,
    TEST_PATH
])
def test_finds_unit_test_directory(path):
    assert file_relations.get_unit_test_directory(path) == TEST_DIRECTORY

import pytest

from lookaside.clients import local


@pytest.fixture
def mock_connection(mocker):
    return mocker.MagicMock(spec=local.Client)

import pathlib

import lookaside.gtest.filter as gtest_filter


TEST_PATH = pathlib.Path.home() / 'code' / 'dev' / 'src' / 'lib' / 'queue' / 'test' / 'TankClientTest.cpp'

def test_finds_all():
    filters = gtest_filter.get_filters_for_tests_in(TEST_PATH)

    expected_filters = ':'.join([
        '*TankClientTest.writeFailsFast*',
        '*TankClientTest.writeWithoutTopic*',
        '*TankClientTest.writeWithTopic*',
        '*TankClientTest.repeatedWrites*',
        '*TankClientTest.clientQueuesRequests*',
        '*TankClientTest.readFailsFast*',
        '*TankClientTest.failFastWorksRetroactively*',
        '*TankClientTest.readSingleValue*',
        '*TankClientTest.throwsBoundaryFaultBeyondLastIndex*',
        '*TankClientTest.readIndex*',
        '*TankClientTest.readMaxBytes*',
        '*TankClientTest.stream*',
        '*TankClientTest.simpleStream*',
        '*TankClientTest.streamFailures*',
        '*TankClientTest.streamStartingNow*',
        '*TankClientTest.streamsFromFirstAvailableIndexWhenStartingBefore*',
        '*TankClientTest.streamsFromFirstAvailableIndexWhenStartingAfter*',
        '*TankClientTest.fulfillsAllWithClientShutdownDownException*',
        '*TankClientTest.shutsDownWithinOneSecond*'
    ])

    assert filters == expected_filters
import pytest

from lookaside.operators import compiler
from lookaside.codebase import cmake


class TestFlags:

    def test_produces_clang_flag(self):
        flags = compiler.Flags(use_clang=True)
        assert flags.list == ['-clang']

    def test_assumes_gcc_flag(self):
        flags = compiler.Flags(use_clang=False)
        assert flags.list == []


class TestCompiler:

    @pytest.fixture
    def fake_targets(self):
        return cmake.Targets([cmake.Target('test')])

    def test_default_compile(self, mock_connection, fake_targets):
        comp = compiler.Compiler(mock_connection)
        comp.compile(fake_targets)

        mock_connection.execute_and_print.assert_called_once_with('build test')

    def test_compiles_unity(self, mock_connection, fake_targets):
        flags = compiler.Flags(use_unity=True)
        comp = compiler.Compiler(mock_connection, flags=flags)
        comp.compile(fake_targets)

        mock_connection.execute_and_print.assert_called_once_with(
            'build test_unity'
        )

    def test_compiles_with_clang(self, mock_connection, fake_targets):
        flags = compiler.Flags(use_clang=True)
        comp = compiler.Compiler(mock_connection, flags=flags)
        comp.compile(fake_targets)

        mock_connection.execute_and_print.assert_called_once_with(
            'build -clang test'
        )

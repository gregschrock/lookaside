import pathlib

import pytest

import lookaside.operators.router


class TestRouter:

    @pytest.fixture
    def router(self, fs):
        return lookaside.operators.router.Router(pathlib.Path('./config'))

    def test_begins_empty(self, router):
        assert len(router.endpoints()) == 0
        assert len(router.repos()) == 0

    def test_creates_repos(self, router):
        router.create_repo('dev', './code/dev')
        assert len(router.repos()) == 1

    def test_creates_endpoints(self, router):
        router.create_endpoint('dev', 'vm1', 9632)
        assert len(router.endpoints()) == 1

    def test_deletes_repos(self, router):
        router.create_repo('dev', './code/dev')
        router.delete_repo('dev')
        assert len(router.repos()) == 0

    def test_deletes_endpoints(self, router):
        router.create_endpoint('dev', 'vm1', 9632)
        router.delete_endpoint('dev')
        assert len(router.endpoints()) == 0

    def test_retrieves_repo_name_from_path(self, router):
        router.create_repo('dev', pathlib.Path('./code/dev'))
        assert router.get_repo_for_path(pathlib.Path('./code/dev')) == 'dev'

    def test_retrieves_repo_name_for_subdirectory(self, router):
        router.create_repo('dev', pathlib.Path('./code/dev'))
        assert router.get_repo_for_path(
            pathlib.Path('./code/dev/subdir')) == 'dev'

    def test_raises_key_error_for_unknown_repo_path(self, router):
        router.create_repo('dev', pathlib.Path('./code/dev'))

        with pytest.raises(KeyError):
            router.get_repo_for_path(pathlib.Path('./non-code'))

    def test_associates_repo_and_endpoint_names(self, router):
        router.create_repo('dev', './code/dev')
        router.create_endpoint('dp', 'vm1', 9632)
        router.associate('dev', 'dp')

        assert router.get_repo_endpoint_name('dev') == 'dp'

    def test_associates_repos_and_endpoints(self, router):
        router.create_repo('dev', './code/dev')
        router.create_endpoint('dp', 'vm1', 9632)
        router.associate('dev', 'dp')

        endpoint = router.get_repo_endpoint('dev')
        assert endpoint.host == 'vm1'
        assert endpoint.port == 9632

    def test_associates_repos_and_endpoints(self, router):
        router.create_repo('dev', './code/dev')
        router.create_endpoint('dp', 'vm1', 9632)
        router.associate('dev', 'dp')

        endpoint = router.get_repo_endpoint('dev')
        assert endpoint.host == 'vm1'
        assert endpoint.port == 9632

    def test_associates_paths_and_endpoints(self, router):
        router.create_repo('dev', './code/dev')
        router.create_endpoint('dp', 'vm1', 9632)
        router.associate('dev', 'dp')

        endpoint = router.get_endpoint_for_path('./code/dev')
        assert endpoint.host == 'vm1'
        assert endpoint.port == 9632

    def test_associates_subpaths_and_endpoints(self, router):
        router.create_repo('dev', './code/dev')
        router.create_endpoint('dp', 'vm1', 9632)
        router.associate('dev', 'dp')

        endpoint = router.get_endpoint_for_path('./code/dev/subpath')
        assert endpoint.host == 'vm1'
        assert endpoint.port == 9632

    def test_raises_key_error_for_unknown_endpoint_paths(self, router):
        router.create_repo('dev', './code/dev')
        router.create_endpoint('dp', 'vm1', 9632)
        router.associate('dev', 'dp')

        with pytest.raises(KeyError):
            router.get_endpoint_for_path('./code/i95')

    def test_creates_list_of_repos_referencing_an_endpoint(self, router):
        router.create_repo('dev', './code/dev')
        router.create_repo('i95', './code/dev')

        router.create_endpoint('dp', 'vm1', 9632)

        router.associate('dev', 'dp')
        router.associate('i95', 'dp')

        assert router.get_repos_referencing_endpoint('dp') == ['dev', 'i95']

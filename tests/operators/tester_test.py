import pytest

from lookaside.operators import tester


class TestCompiler:

    def test_validates_test(self, mock_connection):
        tester.run_targets(mock_connection, ['test'])
        mock_connection.execute_and_print.assert_called_once_with(
            'validate -c -run test'
        )

    def test_validates_multiple_tests(self, mock_connection):
        tester.run_targets(mock_connection, ['test', 'another_test'])

        mock_connection.execute_and_print.assert_called_once_with(
            'validate -c -run test another_test'
        )

    def test_applies_filters(self, mock_connection):
        tester.run_targets(mock_connection, ['test'], filter='some filter')

        mock_connection.execute_and_print.assert_called_once_with(
            'validate -c -f "some filter" -run test'
        )

    def test_can_use_valgrind(self, mock_connection):
        tester.run_targets(mock_connection, ['test'], valgrind=False)
        mock_connection.execute_and_print.assert_called_once_with(
            'validate -c -V -run test'
        )

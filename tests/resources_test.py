import pathlib
import pytest

from lookaside.operators import resources


class TestEndpoint:

    def test_by_name(self):
        endpoint = resources.Endpoint.create_by_name(hostname='host', port=30)

        assert endpoint.host == 'host'
        assert endpoint.port == 30

    def test_by_name(self):
        endpoint = resources.Endpoint.create_by_ip(ip_address='127.0.0.1', port=40)

        assert endpoint.host == '127.0.0.1'
        assert endpoint.port == 40

    def test_default_ports(self):
        assert resources.Endpoint('host').port == 22
        assert resources.Endpoint.create_by_name(hostname='host').port == 22
        assert resources.Endpoint.create_by_ip(ip_address='host').port == 22


class TestRepo:

    def test_contains_root_directory(self):
        repo = resources.Repo(pathlib.Path('./a_directory'))
        assert repo.contains(pathlib.Path('./a_directory'))

    def test_contains_subdirectory(self):
        repo = resources.Repo(pathlib.Path('./a_directory'))
        assert repo.contains(pathlib.Path('./a_directory/subdirectory'))

    def test_does_not_contain_external_directory(self):
        repo = resources.Repo(pathlib.Path('./a_directory'))
        assert not repo.contains(pathlib.Path('./another_directory'))


class TestMachines:

    def test_throws_on_invalid_retrieval(self):
        machines = resources.Machines()

        with pytest.raises(KeyError):
            machines.get('dev')

    def test_returns_valid_machine(self):
        machines = resources.Machines()
        machines.add('dev', resources.Endpoint(host='vm1', port=3435))

        machine = machines.get('dev')
        assert machine.host == 'vm1'
        assert machine.port == 3435

    def test_removes_machine(self):
        machines = resources.Machines()
        machines.add('dev', resources.Endpoint(host='vm1', port=3435))
        machines.remove('dev')

        with pytest.raises(KeyError):
            machines.get('dev')

    def test_persists_to_file(self, fs):
        path = pathlib.Path('./test_file')

        initial_machines = resources.Machines()
        initial_machines.add('dev', resources.Endpoint(host='vm1', port=3435))

        assert not path.exists()
        initial_machines.save(path)
        assert path.exists()

        loaded_machines = resources.Machines()
        loaded_machines.load(path)

        assert len(loaded_machines) == 1

        machine = loaded_machines.get('dev')
        assert machine.host == 'vm1'
        assert machine.port == 3435

    def test_empty_on_load_from_missing_file(self, fs):
        path = pathlib.Path('./test_file')

        machines = resources.Machines()

        assert len(machines) == 0
        machines.load(path)
        assert len(machines) == 0


class TestRepos:

    def test_throws_on_invalid_retrieval(self):
        repos = resources.Repos()

        with pytest.raises(KeyError):
            repos.get('dev')

    def test_returns_valid_machine(self):
        repos = resources.Repos()
        repos.add('dev', resources.Repo(path='fake-path'))

        repo = repos.get('dev')
        assert repo.path == 'fake-path'

    def test_removes_machine(self):
        repos = resources.Repos()
        repos.add('dev', resources.Repo(path='fake-path'))
        repos.remove('dev')

        with pytest.raises(KeyError):
            repos.get('dev')

    def test_persists_to_file(self, fs):
        path = pathlib.Path('./test_file')

        repos = resources.Repos()
        repos.add('dev', resources.Repo(path='fake-path'))

        assert not path.exists()
        repos.save(path)
        assert path.exists()

        assert len(repos) == 1
        repos.remove('dev')
        assert len(repos) == 0

        repos.load(path)
        assert len(repos) == 1

        repo = repos.get('dev')
        assert repo.path == 'fake-path'

    def test_empty_on_load_from_missing_file(self, fs):
        path = pathlib.Path('./test_file')

        repos = resources.Repos()

        assert len(repos) == 0
        repos.load(path)
        assert len(repos) == 0
 
 
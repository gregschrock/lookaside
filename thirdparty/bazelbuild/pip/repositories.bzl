load("@com_apt_itude_rules_pip//rules:repository.bzl", "pip_repository")


def pip_repositories():
    pip_repository(
        name = "pip",
        python_interpreter = "python3.6",
        requirements_per_platform = {
            "//thirdparty/bazelbuild/pip:requirements.txt": "osx",
        },
    )